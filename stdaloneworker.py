
import numpy as np


# imports for retreiving data from server
import multiprocessing as mp
import socket

import time
import select

import psutil
import os

mp.allow_connection_pickling()

# init_worker1:
def init_worker1(arr_in, data_sck, cmd_sck, lk_in):
    # sum_img: shared array for real space image
    # data_sck: data socket
    # lk_in: multiprocessing lock
    global sum_img, s_data, llkk, s_cmd
    sum_img = arr_in
    s_data = data_sck
    s_cmd = cmd_sck
    llkk = lk_in

    p = psutil.Process(os.getpid())
    p.nice(psutil.REALTIME_PRIORITY_CLASS)

# pre-calculate the total byte length for one frame
tcpfrmdata_len = 15+384+131072
# define the multiprocessing worker,
# This worker f'n prints out, for debug purpose, the length of received data.
def sum_worker1():
    global tcpfrmdata_len

    # s_cmd.sendall(MPX_CMD('GET','DETECTORSTATUS'))
    # status = s_cmd.recv(1024)
    # print('Status CMD:', status.decode())


    llkk.acquire() # acquire lock for the data socket.
    # if the following line is used to check the availability of the data,
    # then indent the s_data.recv line.
    # ready = select.select([s_data], [], [], ) # check if there is data is available
    # if ready[0]:
    rawbytes = s_data.recv(tcpfrmdata_len)

    # s_cmd.sendall(MPX_CMD('GET','DETECTORSTATUS'))
    # status = s_cmd.recv(1024)
    # print('Status CMD:', status.decode())

    # print(f'worker {mp.current_process()} received {len(rawbytes)} bytes')
    while (len(rawbytes)!= tcpfrmdata_len):
        # time.sleep(.5)

        # s_cmd.sendall(MPX_CMD('GET','DETECTORSTATUS'))
        # status = s_cmd.recv(1024)
        # print('Status CMD:', status.decode())

        rawbytes += s_data.recv(tcpfrmdata_len-len(rawbytes))
        # print(f'IN WHILE loop worker {mp.current_process()} received {len(rawbytes)} bytes')
    llkk.release() # release the lock for data socket.
    framedata = rawbytes[15:]
    framesum = np.frombuffer(rawbytes[15+384:], dtype='>u2', count=-1)
    # sum_img[int(framedata[4:10].decode())] = int(framedata[4:10].decode())
    sum_img[int(framedata[4:10].decode())-1] = np.sum(framesum, dtype='int64')
    # print(f'worker {mp.current_process()} finished frme {int(framedata[4:10].decode())}')


def MPX_CMD(type_cmd='GET',cmd='DETECTORSTATUS'):
    '''Generate TCP command string for Merlin software.
    type_cmd and cmd are documented in MerlinEM Documentation.
    Default value GET,DETECTORSTATUS probes for the current status of the detector.'''
    length = len(cmd)
    tmp = 'MPX,00000000' + str(length+5) + ',' + type_cmd + ',' + cmd
    return tmp.encode()


def stdaloneworker3(s_cmd, s_data, numberFrames):
# def merlin_take_data(aa, ):
    # global HOST, COMMANDPORT, s_cmd, s_data, numberFrames

    sum_img = mp.Array('Q', numberFrames) # use 'Q' for unsigned long long
    l = mp.Lock()
    s_cmd.sendall(MPX_CMD('CMD','STARTACQUISITION'))

    # s_data_ = s_data
    data = s_data.recv(14)
    # ready = select.select([s_data], [], [], )
    # if ready[0]:
        # rawbytes = s_data.recv(tcpfrmdata_len)
        # data = s_data.recv(14)
    start = data.decode()
    header = s_data.recv(int(start[4:]))
    if (len(header)==int(start[4:])):
        print("Header data received.")


    with mp.Pool(mp.cpu_count()-4, initializer=init_worker1, initargs=(sum_img, s_data, s_cmd, l)) as pool:
        prcs_all = []
        for ii in range(numberFrames):
            r = pool.apply_async(sum_worker1, args=())
            r.get()
            # pool.apply(sum_worker1, args=())
        # for r in prcs_all:
        #     r.get()
            # r.wait()
        # prcs_all[-1].wait()
        # prcs_all[-1].get()
    print(list(sum_img))

    return None
