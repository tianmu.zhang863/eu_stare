########################################
########################################
# STARE project 4D-STEM with SimplexMax factorizartion file
#
# By Tianmu Zhang, Alex Zintler, Lucas Brauch, Tianshu Jiang, Oscar Recalde and Leopoldo Molina-Luna
# AEM @ TU Darmstadt
# tianmu.zhang@tu-darmstadt.de
# alex.zintler@aem.tu-darmstadt.de

# imports for dash and plotly
import dash
import dash_core_components as dcc
import dash_html_components as html
# import dash_design_kit as ddk
# from dash.dependencies import Input, State, ALL, MATCH, Output
from dash_extensions.enrich import Output, DashProxy, Input, MultiplexerTransform, State
# from dash_extensions.enrich import Output
import plotly.express as px
import plotly.graph_objects as go

# import dash bootstrap for style
import dash_bootstrap_components as dbc

import dash_daq as ddaq

# imports for other utility parts
import numpy as np
import json


# imports for retreiving data from server
import multiprocessing as mp
import socket
from multiprocessing import Process, Pipe

import time
import select

# import psutil
import os

# import stdaloneworker
import data_worker

mp.allow_connection_pickling()


# define function for send control command to Merlin
def MPX_CMD(type_cmd='GET',cmd='DETECTORSTATUS'):
    '''Generate TCP command string for Merlin software.
    type_cmd and cmd are documented in MerlinEM Documentation.
    Default value GET,DETECTORSTATUS probes for the current status of the detector.'''
    length = len(cmd)
    tmp = 'MPX,00000000' + str(length+5) + ',' + type_cmd + ',' + cmd
    return tmp.encode()

# figure/image displaying options, this is used for interactive tools on the figure
# in ML section, this will be useful when selecting an area on the real-space image
config = {
    "modeBarButtonsToAdd": [
        "drawline",
        "drawopenpath",
        "drawclosedpath",
        "drawcircle",
        "drawrect",
        "eraseshape",
    ]
}

############################
# definition of tabs for the main interface

# Merlin control tab
tab_merlincontrol = dbc.Card(
    dbc.CardBody(
        [
            html.H4("Settings for Merlin Detector", className="card-title"),
            html.H6("Use the Configure button to set Merlin", className="card-subtitle"),
            html.P("For real-space image size, see Data Acq. tab", className="card-text"),

            # Row with Card for HVBias, Threshold0 and counterdepth
            dbc.Card(
                [
                dbc.CardHeader(" "),
                dbc.CardBody(
                
                    dbc.Row(
                        [
                            dbc.Col(dbc.Card(
                                [
                                    dbc.CardHeader("HV Bais"),
                                    dbc.CardBody(
                                        [
                                            html.H5(dcc.Input(id='setHVBIAS', type='text', value='0'),
                                                        className="card-title",),
                                                # html.P(
                                                #     "This is some card content that we'll reuse",
                                                #     className="card-text",
                                                # ),
                                        ]),
                                ],
                                            color="primary", outline=True,)),
                            dbc.Col(dbc.Card(
                                [
                                    dbc.CardHeader("Threshold 0"),
                                    dbc.CardBody(
                                        [
                                            html.H5(dcc.Input(id='setTHRESHOLD0', type='text', value='0',),
                                                    className="card-title",)
                                        ]),
                                ],
                                color="secondary", outline=True)),

                            dbc.Col(dbc.Card(
                                [
                                    dbc.CardHeader("Counter Depth"),
                                    dbc.CardBody(
                                        [
                                            html.H5(dcc.Input(id='setCOUNTERDEPTH', type='text', value='12'),
                                            )
                                        ]
                                    )
                                ],
                                color="info", outline=True)),                             
                        ],
                        className="mb-4",
                    ),

                )]
            ),

            # Row with Card for ACQ time and ACQ period
            dbc.Card(
                [
                dbc.CardHeader(" "),
                dbc.CardBody(
                
                    dbc.Row(
                        [
                            dbc.Col(dbc.Card(
                                [
                                    dbc.CardHeader("Acqusition Time"),
                                    dbc.CardBody(
                                        [
                                            html.H5(dcc.Input(id='setACQUISITIONTIME', type='text', value='0.8'),
                                                        className="card-title",),
                                        ]),
                                ],
                                            color="primary", outline=True,)),
                            dbc.Col(dbc.Card(
                                [
                                    dbc.CardHeader("Acqusition Period"),
                                    dbc.CardBody(
                                        [
                                            html.H5(dcc.Input(id='setACQUISITIONPERIOD', type='text', value='0.8'),
                                                    className="card-title",)
                                        ]),
                                ],
                                color="secondary", outline=True)),                     
                        ],
                        className="mb-4",
                    ),

                )]
            ),

            # Row with dropdown for triggering selection
            dbc.Card(
                [
                dbc.CardHeader("select start trigger mode"),
                dbc.CardBody(
                dcc.Dropdown(
                    id='triggers_dropdown',
                    options=[
                        {'label': '0 Internal', 'value': '0'},
                        {'label': '1 Rising Edge (TTL)', 'value': '1'},
                        {'label': '2 Falling Edge (TTL)', 'value': '2'},
                        {'label': '3 Rising Edge (LVDS)', 'value': '3'},
                        {'label': '4 Falling Edge (LVDS)', 'value': '4'},
                        {'label': '5 Soft Trigger', 'value': '5'},
                        {'label': '6 Multi-Gate Rising Edge (TTL)', 'value': '6'},
                        {'label': '7 Multi-Gate Falling Edge (TTL)', 'value': '7'},
                        {'label': '8 Multi-Gate Rising Edge (LVDS)', 'value': '8'},
                        {'label': '9 Multi-Gate Falling Edge (LVDS)', 'value': '9'},
                        {'label': '10 Multi-Gate Soft Trigger', 'value': '10'},

                    ],
                    value='0'
                ),
                )]),
            dbc.Button("Configure Merlin", color="primary", id='config_serv_button1'),
            dbc.Container([html.Hr(className="display-10"),], className="py-3",),
            # Card for Merlin status
            dbc.Card(
                [
                dbc.CardHeader("Merlin Status"),
                dbc.CardBody(
                    dbc.Table([
                        html.Thead(html.Tr([html.Th("Name"), html.Th("Status")])),
                        html.Tbody(
                            [
                                html.Tr([html.Td('HVBias'), html.Td(html.Div(id='getHVBIAS', style={'whiteSpace': 'pre-line'}))]),
                                html.Tr([html.Td('Threshold 0'), html.Td(html.Div(id='getTHRESHOLD0', style={'whiteSpace': 'pre-line'}))]),
                                html.Tr([html.Td('Counter Depth'), html.Td(html.Div(id='getCOUNTERDEPTH', style={'whiteSpace': 'pre-line'}))]),
                                html.Tr([html.Td('Acqusition Time'), html.Td(html.Div(id='getACQUISITIONTIME', style={'whiteSpace': 'pre-line'}))]),
                                html.Tr([html.Td('Acqusition Period'), html.Td(html.Div(id='getACQUISITIONPERIOD', style={'whiteSpace': 'pre-line'}))]),
                                html.Tr([html.Td('Number of Frames'), html.Td(html.Div(id='getNUMFRAMESTOACQUIRE', style={'whiteSpace': 'pre-line'}))]),
                                html.Tr([html.Td('Trigger Mode'), html.Td(html.Div(id='getTriggermode', style={'whiteSpace': 'pre-line'}))]),
                            ]
                        )
                    ], bordered=True, striped=True, hover=True)
                )]),
        ]
    ),
    className="mt-3",
)

# DAQ tab
tab_dataacq = dbc.Card(
    dbc.CardBody(
        [
            html.H4("Settings for Data Acquisition for Merlin Detector", className="card-title"),
            html.H6("Use the Configure button to set Merlin", className="card-subtitle"),
            # html.P("For real-space image size, see Data Acq. tab", className="card-text"),

            # Row with Card for ACQ time and ACQ period
            # Card for real space dimension and number of reciprocal space frames
            dbc.Card(
                [
                dbc.CardHeader("Acquisition timing"),
                dbc.CardBody(
                    dbc.Row(
                        [
                            dbc.Col(dbc.Card(
                                [
                                    dbc.CardHeader("number of lines:"),
                                    dbc.CardBody(
                                        [
                                            html.H5(dcc.Input(id='num_real_img_lines', type='number', 
                                                            placeholder='number of lines', value=256),
                                                    className="card-title",),
                                        ]),
                                ],
                                            color="primary", outline=True,)),
                            dbc.Col(dbc.Card(
                                [
                                    dbc.CardHeader("number of frames per lines:"),
                                    dbc.CardBody(
                                        [
                                            html.H5(dcc.Input(id='num_real_frm_perlines', type='number',
                                                            placeholder='number of frames', value=256),
                                                    className="card-title",)
                                        ]),
                                ],
                                color="secondary", outline=True)),                     
                        ],
                        className="mb-4",
                    ),

                )]
            ),
            # Card for contiuous RW and Merlin saving file
            dbc.Card(
                [
                dbc.CardHeader(" "),
                dbc.CardBody(
                    dbc.Row(
                        [
                            dbc.Col(dbc.Card(
                                [
                                    dbc.CardHeader("Continuous RW:"),
                                    dbc.CardBody(
                                        [
                                            html.H5(dcc.RadioItems(id='setCONTINUOUSRW',
                                                                options=[
                                                                    {'label': 'no', 'value': '0'},
                                                                    {'label': 'yes', 'value': '1'},
                                                                ],
                                                                value='1', 
                                                                # labelStyle={'display': 'inline-block'},
                                                                labelStyle = {'display': 'block', 'cursor': 'pointer', 'margin-left':'20px'},
                                                                inputStyle={"margin-right": "20px"}
                                                            ),
                                                    className="card-title",),
                                        ]),
                                ],
                                            color="primary", outline=True,)),

                            dbc.Col(dbc.Card(
                                [
                                    dbc.CardHeader("number of frames per lines:"),
                                    dbc.CardBody(
                                        [
                                            html.H5(dcc.RadioItems(id='setFILESAVING',
                                                                options=[
                                                                    {'label': 'no', 'value': '0'},
                                                                    {'label': 'yes', 'value': '1'},
                                                                ],
                                                                value='0', 
                                                                # labelStyle={'display': 'inline-block'},
                                                                labelStyle = {'display': 'block', 'cursor': 'pointer', 'margin-left':'20px'},
                                                                inputStyle={"margin-right": "20px"}
                                                            ),

                                                    className="card-title",)
                                        ]),
                                ],
                                color="secondary", outline=True)),                     
                        ],
                        className="mb-4",
                    ),
                    

                ),
                ]
            ),
            # Card for displaying total number of reciprocal space frame to take
            dbc.Card(
                [
                dbc.CardHeader("Total number of reciprocal space frames to be taken"),
                dbc.CardBody(
                    dcc.Input(id='setNUMFRAMESTOACQUIRE', type='text', value='65536'),
                )]),
            # Card for saving data file through the interface and file path
            dbc.Card(
                [
                dbc.CardHeader("Interface data saving"),
                dbc.CardBody(
                    html.Div(children=[
                            html.Div(children=[
                                'Interface 4d data saving',
                                ' No need to add suffix, will be saved as .pkl',
                                dcc.Input(id='interfaceFilePath', type='text',
                                value='.\data_save\\test_data'),
                                dbc.Button(id='interfaceSaveData', n_clicks=0,
                                children=['Save 4D data as pkl'], color='success' ),
                            ]
                            )
                        ]),
                )
                ]
            ),
            # Button configure Merlin
            # dbc.Button("Configure Merlin", color="primary"),
            dbc.Button("Configure Merlin", color="primary", id='config_serv_button2'),
            dbc.Container([html.Hr(className="display-10"),], className="py-3",),
            # Card for Merlin status displaying
            dbc.Card(
                [
                dbc.CardHeader("Merlin Status"),
                dbc.CardBody(
                    dbc.Table([
                        html.Thead(html.Tr([html.Th("Name"), html.Th("Status")])),
                        html.Tbody(
                            [
                                html.Tr([html.Td('HVBias'), html.Td(html.Div(id='getHVBIAS_1', style={'whiteSpace': 'pre-line'}))]),
                                html.Tr([html.Td('Threshold 0'), html.Td(html.Div(id='getTHRESHOLD0_1', style={'whiteSpace': 'pre-line'}))]),
                                html.Tr([html.Td('Counter Depth'), html.Td(html.Div(id='getCOUNTERDEPTH_1', style={'whiteSpace': 'pre-line'}))]),
                                html.Tr([html.Td('Acqusition Time'), html.Td(html.Div(id='getACQUISITIONTIME_1', style={'whiteSpace': 'pre-line'}))]),
                                html.Tr([html.Td('Acqusition Period'), html.Td(html.Div(id='getACQUISITIONPERIOD_1', style={'whiteSpace': 'pre-line'}))]),
                                html.Tr([html.Td('Number of Frames'), html.Td(html.Div(id='getNUMFRAMESTOACQUIRE_1', style={'whiteSpace': 'pre-line'}))]),
                                html.Tr([html.Td('Trigger Mode'), html.Td(html.Div(id='getTriggermode_1', style={'whiteSpace': 'pre-line'}))]),
                            ]
                        )
                    ], bordered=True, striped=True, hover=True)
                )]),
        ]
    ),
    className="mt-3",
)

# Machine learning tab
tab_ML = dbc.Card(
    dbc.CardBody(
        [
            dbc.Accordion([
                dbc.AccordionItem(
                    [
                        dbc.Button(id='LoadDeepModel', n_clicks=0, children=["Load deep model"], color="danger"),
                        dbc.Button(id='LoadExampleImageDeep', n_clicks=0, children=["Load example image"], color="danger"),
                        dcc.Graph(id='atomseg_realimg', config=config),
                        dcc.Input(id='AtomPosFilePath', type='text', value='.\data_save\\atompos'),
                        dbc.Button(id='rundeepatomseg', n_clicks=0, children=["Identify atoms"], color="danger"),
                        dbc.Button(id='SaveDeepAtomPos', n_clicks=0, children=["Save atom positions to npy"], color="danger"),
                        dcc.Graph(id='atomseg_realimg_out', config=config),
                    ],
                    title='Deep learning for atom positisons etrieval'
                ),
                dbc.AccordionItem(
                    [   
                        # dbc.DropdownMenu(
                        #     id='realimg_dropdown',
                        #     # label="Choose virtual detector for real-space image",
                        #     children=[
                        #         dbc.DropdownMenuItem(divider=True),
                        #         dbc.DropdownMenuItem("BF"),
                        #         dbc.DropdownMenuItem("ABF"),
                        #         dbc.DropdownMenuItem("ADF"),
                        #         dbc.DropdownMenuItem("HAADF"),
                        #     ],
                        # ),

                        dcc.Dropdown(
                                id='realimg_dropdown',
                                options=[
                                    {'label': '0 BF', 'value': 'BF'},
                                    {'label': '1 ABF', 'value': 'ABF'},
                                    {'label': '2 ADF', 'value': 'ADF'},
                                    {'label': '3 HAADF', 'value': 'HAADF'},
                                ], value='0'
                            ),
                        dbc.Button("Load sample data with image", id='loadsamplesivm', color="danger", n_clicks=0),
                        # dcc.Graph(id='selected_realimg', config=config),
                        html.Div(
                            children =[
                            #################################
                            # two real-space images side by side, one for selecting decomposition region,
                            # the other for selecting projection region.
                            html.Div(style={'width': '45%', 'height': '45%', 'display': 'inline-block'},
                            children = [html.Center('select decompo. region'),dcc.Graph(id="selected_realimg2", config=config,),
                            dcc.Graph(id='recpimg'),
                            ]),
                            html.Div(style={'width': '45%', 'height': '45%', 'display': 'inline-block'},
                            children=[html.Center('select project. region'), dcc.Graph(id="selected_realimg3", config=config,
                            )])
                            ]
                            ),
                        # html.P("This is tab 2!", className="card-text"),
                        # dbc.Button("Don't click here", color="danger"),
                        # input for selecting decompostion dimensions
                        html.Div(
                        children=['number of \n eigen/reduce_dim: ', 
                        dbc.Input(id='num_eig', type='number', placeholder='number of dim.', value=3)]),
                        # Button for start sivm decomposition
                        dbc.Button(id='sivm_button', n_clicks=0, children=[
                            html.Div(children=[html.Div(
                            children=['''x left: ''', html.Div(id = 'x_left', style={'display': 'inline-block'})], style={'display': 'inline-block'}),
                            html.Div(id='intermediate_value_x_l', style={'display': 'none'})]),

                            html.Div(children=[html.Div(
                            children=['''x right: ''', html.Div(id = 'x_right', style={'display': 'inline-block'})], style={'display': 'inline-block'}),
                            html.Div(id='intermediate_value_x_r', style={'display': 'none'})]),

                            html.Div(children=[html.Div(
                            children=['''y bottom: ''', html.Div(id = 'y_top', style={'display': 'inline-block'})], style={'display': 'inline-block'}),
                            html.Div(id='intermediate_value_y_b', style={'display': 'none'})]),

                            html.Div(children=[html.Div(
                            children=['''y top: ''', html.Div(id = 'y_bottom', style={'display': 'inline-block'})], style={'display': 'inline-block'}),
                            html.Div(id='intermediate_value_y_t', style={'display': 'none'})]),

                            html.Div(children=['perform decomposition'])
                            ]),
                        # Button for sivm projection
                        dbc.Button(id='proj_button', n_clicks=0, children=[
                            html.Div(children=[html.Div(
                            children=['''x left: ''', html.Div(id = 'x_left_p', style={'display': 'inline-block'})], style={'display': 'inline-block'}),
                            html.Div(id='intermediate_value_x_l_p', style={'display': 'none'})]),

                            html.Div(children=[html.Div(
                            children=['''x right: ''', html.Div(id = 'x_right_p', style={'display': 'inline-block'})], style={'display': 'inline-block'}),
                            html.Div(id='intermediate_value_x_r_p', style={'display': 'none'})]),

                            html.Div(children=[html.Div(
                            children=['''y bottom: ''', html.Div(id = 'y_top_p', style={'display': 'inline-block'})], style={'display': 'inline-block'}),
                            html.Div(id='intermediate_value_y_b_p', style={'display': 'none'})]),

                            html.Div(children=[html.Div(
                            children=['''y top: ''', html.Div(id = 'y_bottom_p', style={'display': 'inline-block'})], style={'display': 'inline-block'}),
                            html.Div(id='intermediate_value_y_t_p', style={'display': 'none'})]),

                            html.Div(children=['perform projection'])
                            ]),

                        # List of decompostion and projection images, side by side
                        html.Div(
                            children = [
                                html.Div(style={'width': '45%', 'height': '45%', 'display': 'inline-block'},
                                    children=[
                                        html.Div('List of eigen images'),
                                            # html.Div(id='ListOfEigen', children=[])
                                            html.Div(id='ListOfEigen',)
                                            ]),
                                        html.Div(style={'width': '45%', 'height': '45%', 'display': 'inline-block'},
                                            children=[
                                            html.Div('List of projection images'),
                                            # html.Div(id='ListOfEigen', children=[])
                                            html.Div(id='ListOfProj',)
                                            ])
                                        ]
                                )
                    ],
                    title='Factorization for Phase Mapping'),
            ])
        ]
    ),
    className="mt-3",
)

# images and virtual detector tab
tab_realspaceimgs = dbc.Card(
    dbc.CardBody(
        [
            # accordion for virtual detector setting and image display
            dbc.Accordion([
                # accordion item for virtual detector radii selection
                dbc.AccordionItem(
                    [
                        # card for virtual detector radii selection
                        dbc.Card(
                            [
                                dbc.CardHeader("Virtual detector definitions"),
                                dbc.CardBody(
                                    [
                                    # card for BF virtual detector range
                                    dbc.Card(
                                        [
                                            dbc.CardHeader("BF radii (pixels)"),
                                            dbc.CardBody([
                                                dcc.RangeSlider(
                                                    id='BFVD-range-slider',
                                                    min=0,
                                                    max=120,
                                                    step=1,
                                                    value=[0, 15],
                                                    marks={
                                                        0:{'label':'0', 'style': {'color': '#ffffff'}},
                                                        25:{'label':'25', 'style': {'color': '#ffffff'}},
                                                        50:{'label':'50', 'style': {'color': '#ffffff'}},
                                                        75:{'label':'75', 'style': {'color': '#ffffff'}},
                                                        100:{'label':'100', 'style': {'color': '#ffffff'}},
                                                        },
                                                    tooltip={"placement": "bottom", "always_visible": True},    
                                                ),
                                                html.Div(id='output-BFVD-range-slider'),  
                                            ])
                                        ], color='success', inverse=True,
                                    ),

                                    # card for ABF virtual detector range
                                    dbc.Card(
                                        [
                                            dbc.CardHeader("ABF radii (pixels)"),
                                            dbc.CardBody([
                                                dcc.RangeSlider(
                                                    id='ABFVD-range-slider',
                                                    min=0,
                                                    max=120,
                                                    step=1,
                                                    value=[7, 15],
                                                    marks={
                                                        0:{'label':'0', 'style': {'color': '#ffffff'}},
                                                        25:{'label':'25', 'style': {'color': '#ffffff'}},
                                                        50:{'label':'50', 'style': {'color': '#ffffff'}},
                                                        75:{'label':'75', 'style': {'color': '#ffffff'}},
                                                        100:{'label':'100', 'style': {'color': '#ffffff'}},
                                                        },
                                                    tooltip={"placement": "bottom", "always_visible": True},    
                                                ),
                                                html.Div(id='output-ABFVD-range-slider'),  
                                            ])
                                        ], color='info', inverse=True,
                                    ),
                                    # card for ADF virtual detector range
                                    dbc.Card(
                                        [
                                            dbc.CardHeader("ADF radii (pixels)"),
                                            dbc.CardBody([
                                                dcc.RangeSlider(
                                                    id='ADFVD-range-slider',
                                                    min=0,
                                                    value=[17, 64],
                                                    max=120,
                                                    step=1,
                                                    marks={
                                                        0:{'label':'0', 'style': {'color': '#ffffff'}},
                                                        25:{'label':'25', 'style': {'color': '#ffffff'}},
                                                        50:{'label':'50', 'style': {'color': '#ffffff'}},
                                                        75:{'label':'75', 'style': {'color': '#ffffff'}},
                                                        100:{'label':'100', 'style': {'color': '#ffffff'}},
                                                        },
                                                    tooltip={"placement": "bottom", "always_visible": True},    
                                                ),
                                                html.Div(id='output-ADFVD-range-slider'),  
                                            ])
                                        ], color='primary', inverse=True,
                                    ),
                                    # card for HAADF virtual detector range
                                    dbc.Card(
                                        [
                                            dbc.CardHeader("HAADF radii (pixels)"),
                                            dbc.CardBody([
                                                dcc.RangeSlider(
                                                    id='HAADFVD-range-slider',
                                                    min=0,
                                                    max=120,
                                                    step=1,
                                                    value=[64, 100],
                                                    marks={
                                                        0:{'label':'0', 'style': {'color': '#ffffff'}},
                                                        25:{'label':'25', 'style': {'color': '#ffffff'}},
                                                        50:{'label':'50', 'style': {'color': '#ffffff'}},
                                                        75:{'label':'75', 'style': {'color': '#ffffff'}},
                                                        100:{'label':'100', 'style': {'color': '#ffffff'}},
                                                        },
                                                    tooltip={"placement": "bottom", "always_visible": True},    
                                                ),
                                                html.Div(id='output-HAADFVD-range-slider'),  
                                            ])
                                        ], color='warning', inverse=True,
                                    ),

                                    html.Div(
                                        children=[
                                            dbc.Button(id='set_masks', n_clicks=0, children=['set masks'] ),
                                            'Sie mussen set_masks zumindest eimal drucken/',
                                            'presse the set_masks at least one time'
                                            ]),
                                    ],
                                style={"width":"75%"},
                                className='mt-3')]
                        ),
                    ],
                    title='Virtual Detectors Radii Values'
                ),
                # accordion item for virtual detector boolean selection
                dbc.AccordionItem(
                    [
                        # card for virtiual detector boolean switches
                        dbc.Card(
                            [
                                dbc.CardHeader("Virtual detector selection, to be displayed"),
                                dbc.CardBody(
                                    [
                                        "Selecting more than 2 virtual detector may cause too much computatinoal load.",
                                        # dcc.Checklist(
                                        #     id='vd_list',
                                        #     options=[
                                        #         {'label': 'BF', 'value': 'BF'},
                                        #         {'label': 'ABF', 'value': 'ABF'},
                                        #         {'label': 'ADF', 'value': 'ADF'},
                                        #         {'label': 'HAADF', 'value': 'HAADF'}
                                        #     ],
                                        #     # value=['BF', ''],
                                        #     # labelStyle={'display': 'inline-block'}
                                        # ),
                                        # Boolean button for BF image
                                        html.Tr(
                                            [
                                                html.H6("BF",),
                                                html.Td(
                                                    ddaq.BooleanSwitch(
                                                        id='boolean_BFVD',
                                                        on=False,
                                                        color="#9B51E0",
                                                    ),
                                                )
                                            ]
                                        ),
                                        # Boolean button for ABF image
                                        html.Tr(
                                            [
                                                html.H6("ABF", ),
                                                html.Td(
                                                    ddaq.BooleanSwitch(
                                                        id='boolean_ABFVD',
                                                        on=True,
                                                        color="#9B51E0",
                                                    ),
                                                )
                                            ]
                                        ),
                                        # Boolean button for ADF image
                                        html.Tr(
                                            [
                                                html.H6("ADF",),
                                                html.Td(
                                                    ddaq.BooleanSwitch(
                                                        id='boolean_ADFVD',
                                                        on=False,
                                                        color="#9B51E0",
                                                    ),
                                                )
                                            ]
                                        ),
                                        # Boolean button for HAADF image
                                        html.Tr(
                                            [
                                                html.H6("HAADF", 
                                                # style={'padding-right':'5%'},
                                                ),
                                                html.Td(
                                                    ddaq.BooleanSwitch(
                                                        id='boolean_HAADFVD',
                                                        on=True,
                                                        color="#9B51E0",
                                                    ),
                                                )
                                            ]
                                        ),
                                    ]
                                )
                            ]
                        )
                    ],
                    title='Virtual detector selection'
                ),
                # accordion item for real space image displays
                dbc.AccordionItem(
                    [
                        # Button to start taking 4D-STEM data
                        html.Div(
                            children=[
                                    dbc.Button(id='take4Dimage_stdalone', n_clicks=0, children=['take 4d image standalone'] ),
                                ]
                        ),
                        # Button to start taking 4D-STEM data
                        html.Div(
                            children=[
                                dbc.Button(id='take4Dimage_stdalone2', n_clicks=0, children=['take 4d image standalone2'] ),
                            ]
                        ),
                        # card for images
                        dbc.Card(
                            [
                                dbc.CardHeader("Real-space Images (based on selected virtual detectors)"),
                                dbc.CardBody(
                                    [
                                        dbc.Row(
                                            [
                                                dbc.Col(
                                                    # [
                                                    # html.H4('BF'),
                                                    # html.Div(
                                                        dbc.Card([
                                                            dbc.CardHeader("BF"),
                                                            dbc.CardBody([dcc.Graph(id="realimgBF",),],
                                                            # style={'width': '49%',},
                                                        ),
                                                        ]),
                                                        width=6
                                                    # )
                                                    # ]
                                                ),
                                                # html.H4('ABF'),
                                                dbc.Col(
                                                    # html.Div(
                                                        dbc.Card([
                                                            dbc.CardHeader("ABF"),
                                                            dbc.CardBody([dcc.Graph(id='realimgABF',),],
                                                        ),
                                                        ]),
                                                    width=6
                                                ),
# Below is the old code for showing four real space iamges, leaving it here for formatting reference
# html.Div(style={'width': '45%', 'height': '45%', 'display': 'inline-block'},
#                         children = [
#                             html.Center('BF'),
#                             dcc.Graph(id="realimgBF",
#                             # figure = fig1,
#                             # config=config
#                             ),
#                             html.Center('ABF'),
#                             dcc.Graph(id='realimgABF'),
#                             ]),


                                            ]
                                        ),
                                        # second row of real-space images
                                        dbc.Row(
                                            [
                                                dbc.Col(
                                                    # [
                                                    # html.H4('BF'),
                                                    # html.Div(
                                                        dbc.Card([
                                                            dbc.CardHeader("ADF"),
                                                            dbc.CardBody([dcc.Graph(id="realimg_ADF",),],
                                                            # style={'width': '49%',},
                                                        ),
                                                        ]),
                                                        width=6
                                                    # )
                                                    # ]
                                                ),
                                                # html.H4('ABF'),
                                                dbc.Col(
                                                    # html.Div(
                                                        dbc.Card([
                                                            dbc.CardHeader("HAADF"),
                                                            dbc.CardBody([dcc.Graph(id='realimg_HAADF',),],
                                                        ),
                                                        ]),
                                                    width=6
                                                ),
                                            ]
                                        ),
                                    ],
                                ),
                            ]
                        ),
                    ],
                    title='Real-space Images',
                ),

            ]),
            # html.P("Real space images", className="card-text"),
            # dbc.Button("Don't click here", color="danger"),
        ]
    ),
    className="mt-3",
)

# DENS sulotions and JEOL tab will be followed here.

##################################
# main dash app starts from here
# app_main = dash.Dash(__name__, title='STARE AEM TU Darmstadt', 
#                     external_stylesheets=[dbc.themes.MATERIA])
app_main = DashProxy(__name__, title='STARE AEM TU Darmstadt', 
                    external_stylesheets=[dbc.themes.MATERIA],
                    prevent_initial_callbacks=True, transforms=[MultiplexerTransform()])

app_main.layout = html.Div(
    children = [

        # storages for outputs from callback functions.
        # They are here since callback functions need output.
        dcc.Store(id='data_socket_store_configure'),
        dcc.Store(id='data_realspace_images_all'),
        dcc.Store(id='data_socket_store_4ddatasaving'),
        dcc.Store(id='data_socket_store_setmasks'),
        dcc.Store(id='data_socket_store_take4d1'),
        dcc.Store(id='data_socket_store_take4d2'),
        dcc.Store(id='data_socket_store_loaddeepmodel'),
        dcc.Store(id='data_socket_store_temp1'),
        dcc.Store(id='data_socket_store_temp2'),

        dcc.Store(id='memory_eigen_loading'),

        # carousel for rotating pictures
        dbc.Row([
            dbc.Col([
                dbc.Carousel(
                items=[
                    {
                        "key": "1",
                        "src": "/assets/imgs/ARM_200.jpg",
                        "header": "JEOL control",
                        "img_style" :{"width":"400px", "height":"600px"},
                    },
                    {
                        "key": "2",
                        "src": "/assets/imgs/Merlin_0x650.jpg",
                        "header": "Merlin control",
                        "img_style" :{"width":"400px", "height":"600px"},
                    },
                    {
                        "key": "3",
                        "src": "/assets/imgs/Denshalter_klein.jpg",
                        "header": "DENS Solutions in situ control",
                        "img_style" :{"width":"400px", "height":"600px"},
                        # "caption": "This slide has a caption only",
                    },
                ],
                controls=False,
                indicators=False,
                interval=900,
                ride="carousel",
                className="h-3850 p-3 bg-light rounded-3",
                # style={"width":"100%", "height":"500p"}
            ),
            ],
            className="h-8850 p-3 bg-light rounded-3",),
            # add another column here to control the total height,
            # so the height will not change if the pictures sizes are different (in the carousel)
            dbc.Col([" "], width=0.1, className="h-8850 p-3 bg-light rounded-3")
        ]),
        
        # Heading of the main page
        # html.H1('STARE - An all-inclusive suite for in situ experiments and analysis'),
        dbc.Row([
            dbc.Col(
                html.Div(
                    # Container for heading text, AEM logo, TUDarmstad logo and etc.
                    dbc.Container(
                        [
                        html.H1("STARE", className="display-3"),
                        html.H3(
                            "An all-inclusive suite for in situ experiments and analysis",
                            # "featured content or information.",
                            className="lead",
                        ),
                        dbc.Container([html.Hr(className="display-10"),], className="py-3",),
                        html.Div(
                            html.Img(src='/assets/imgs/aem_logo.png', style={'height':'65%', 'width':'65%'}),
                        ),
                        dbc.Container([html.Hr(className="display-10"),], className="py-3",),
                        html.Div(
                            html.Img(src='/assets/imgs/tud_logo.png', style={'height':'65%', 'width':'65%'}),
                        ),
                        # dbc.Container([html.Hr(className="display-10"),], className="py-3",),
                        # dbc.Container([html.Hr(className="display-10"),], className="py-3",),
                        # dbc.Container([html.Hr(className="display-10"),], className="py-3",),
                        # dbc.Container([html.Hr(className="display-10"),], className="py-3",),
                        # dbc.Container([html.Hr(className="display-10"),], className="py-3",),
                        # html.P(
                        #     "Use utility classes for typography and spacing to suit the "
                        #     "larger container."
                        # ),
                        # html.P(
                        #     dbc.Button("Learn more", color="primary"), className="lead"
                        #     ),
                        ],
                        fluid=True,
                        className="py-3",
                    ),
                    className="p-3 bg-light rounded-3",
                ),
                md=4,
            ),
            dbc.Col(
                html.Div(
                    # Container for carousel for ERC, DENS and QD logos
                    dbc.Container(
                        [
                        html.H1("Made possible by", className="display-3"),
                        # carousel for rotating pictures for sponsors
                        dbc.Carousel(
                            items=[
                                {
                                    "key": "1",
                                    "src": "/assets/imgs/erc_logo.png",
                                },
                                {
                                    "key": "2",
                                    "src": "/assets/imgs/Quantum_Detectors_Logo.png",
                                },
                                {
                                    "key": "3",
                                    "src": "/assets/imgs/DENSsolutions-logo-big.png",
                                    # "header": "DENS Solutions in situ control",
                                    # "caption": "This slide has a caption only",
                                },
                                {
                                    "key": "4",
                                    "src": "/assets/imgs/pycrologo.png",
                                    # "header": "Pycroscopy Atomai for atom identification",
                                    "img_style" :{"width":"180px", "height":"300px"},
                                    # "caption": "This slide has a caption only",
                                },
                            ],
                            className="display-3",
                            controls=False,
                            indicators=False,
                            interval=500,
                            ride="carousel",
                        ),
                        # html.Hr(className="my-2"),
                        ],
                        fluid=True,
                        className="py-3",
                    ),
                    className="h-850 p-3 bg-light rounded-3",
                ),
                md=6,
            ),
        ],className="align-items-md-stretch",
        ),
        
        # Main navigation tabs
        dbc.Tabs(
                [
                    dbc.Tab(tab_merlincontrol, label="Merlin control"),
                    dbc.Tab(tab_dataacq, label="Data acqusition"),
                    dbc.Tab(tab_realspaceimgs, label='Real-space images'),
                    dbc.Tab(tab_ML, label="Machine learning"),
                    dbc.Tab('in situ', label="DENS Solution in situ", disabled=True),
                    dbc.Tab(" ", label="JEOL control", disabled=True),
                ]
            ),
        #################################
            

# below are commands for configuring the Merlin, leave here for general testing purpose
# s_cmd.sendall(MPX_CMD('SET','CONTINUOUSRW,1'))
# #Set dynamic range
# s_cmd.sendall(MPX_CMD('SET','COUNTERDEPTH,12'))
# #Set frame time in miliseconds
# s_cmd.sendall(MPX_CMD('SET','ACQUISITIONTIME,0.8'))
# #Set gap time in milliseconds (The number fire corresponds to sum of frame and gap time)
# s_cmd.sendall(MPX_CMD('SET','ACQUISITIONPERIOD,0.8'))
# #Set number of frames to be acquired
# s_cmd.sendall(MPX_CMD('SET','NUMFRAMESTOACQUIRE,'+str(numberFrames)))
# #Disable file saving
# s_cmd.sendall(MPX_CMD('SET','FILEENABLE,0'))

        
        # buttons
        # html.Div(children=[
        #     html.Button(id='config_serv_button', n_clicks=0, children=['Configure Server'])
        #         ]),

        

        # input checkbox for virtual detectors
        # html.Div(children=[
        #     'select virtual detectors',
        # ]),

        ##########################################
        # Buttons
        ##########################################
        # html.Div(children=[
        #     html.Button(id='take4Dimage_para2', n_clicks=0, children=['take 4d image the second way'] ),
        #         ]),
        # html.Div(children=[
        #     html.Button(id='take4Dimage_nonpara', n_clicks=0, children=['take 4d image non parallel'] ),
        #         ]),

        ##########################################
        # Figures
        ##########################################
        # html.Div(
        # # style={'width': '45%', 'height': '45%', 'display': 'inline-block'},
        #     children=[
        #         html.Div('Real space images'),
        #         # html.Div(id='ListOfEigen', children=[])
        #         html.Div(id='realspaceimgs',),

                
        #     ]
        # )
    ]
)
        
        # ])
        # )

# 'setHVBIAS' 'setTHRESHOLD0' 'setCOUNTERDEPTH' 'setACQUISITIONTIME'
#  'setNUMFRAMESTOACQUIRE' 'triggers_dropdown' 'setCONTINUOUSRW' 'setFILESAVING'

# callback for updating number of reciprocal frame to take,
@app_main.callback(
    Output('setNUMFRAMESTOACQUIRE', 'value'),
    Input('num_real_img_lines', 'value'),
    Input('num_real_frm_perlines', 'value'),
    )
def calc_num_recp_img(aa, bb):
    return aa*bb

# Callback for configure Merlin button 
@app_main.callback(
    # Output('data_socket_store_configure', 'data'),
    Output('getHVBIAS', 'children'),
    Output('getTHRESHOLD0', 'children'),
    Output('getCOUNTERDEPTH', 'children'),
    Output('getACQUISITIONTIME', 'children'),
    Output('getACQUISITIONPERIOD', 'children'),
    Output('getNUMFRAMESTOACQUIRE', 'children'),
    Output('getTriggermode', 'children'),
    # Input('config_serv_button', 'n_clicks'),
    Input('config_serv_button1', 'n_clicks'),
    Input('config_serv_button2', 'n_clicks'),
    State('setHVBIAS', 'value'),
    State('setTHRESHOLD0', 'value'),
    State('setCOUNTERDEPTH', 'value'),
    State('setACQUISITIONTIME', 'value'),
    State('setACQUISITIONPERIOD', 'value'),
    State('num_real_frm_perlines', 'value'),
    State('setNUMFRAMESTOACQUIRE', 'value'),
    State('triggers_dropdown', 'value'),
    State('setCONTINUOUSRW', 'value'),
    State('setFILESAVING', 'value'),
    prevent_initial_call=True
)
def configure_server(
    aa, aaa, # these two will receive the number of clicks from two configure merlin buttons.
    setHVBIAS, setTHRESHOLD0, setCOUNTERDEPTH, setACQUISITIONTIME, setACQUISITIONPERIOD,
    num_real_frm_perlines, setNUMFRAMESTOACQUIRE, triggers_dropdown, setCONTINUOUSRW, setFILESAVING):
    # global varibles are needed, so the socket can be kept outside callback
    # functions.
    global HOST, COMMANDPORT, DATAPORT, s_cmd, numberFrames
    global s_data, parent_conn, child_conn

    if 's_cmd' not in globals():
        s_cmd = socket.socket(socket.AF_INET, socket.SOCK_STREAM) # Create command socket
        # Connect sockets and probe for the detector status
        # try:
        s_cmd.connect((HOST, COMMANDPORT))
        #     # s_cmd.sendall(MPX_CMD('GET','SOFTWAREVERSION'))
        #     # version = s_cmd.recv(1024)
        #     # s_cmd.sendall(MPX_CMD('GET','DETECTORSTATUS'))
        #     # status = s_cmd.recv(1024)
        #     # print('Version CMD:', version.decode())
        #     # print('Status CMD:', status.decode())
        # except ConnectionRefusedError:
        #     print("Merlin not responding")
        # except OSError:
        #     print("Merlin already connected")
        # s_cmd.close()


    # Connect data socket
    if 's_data' not in globals():
        s_data = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        # try:
        # s_data.connect((HOST, DATAPORT))
        s_data.connect((HOST, 6342))
        # s_data.setblocking(0)
        # except ConnectionRefusedError:
        #     print("Data port not responding")

    # Set base Merlin imaging parameters
    numberFrames = int(setNUMFRAMESTOACQUIRE) #65536
    nunFrmperTri = int(num_real_frm_perlines)

    # Sending set command to detector
    # s_cmd.sendall(MPX_CMD('SET','HVBIAS,0'))
    s_cmd.sendall(MPX_CMD('SET','HVBIAS,'+setHVBIAS))
    # s_cmd.sendall(MPX_CMD('SET','THRESHOLD0,0'))
    s_cmd.sendall(MPX_CMD('SET','THRESHOLD0,'+setTHRESHOLD0))
    # Set continuous mode on
    # s_cmd.sendall(MPX_CMD('SET','CONTINUOUSRW,1'))
    s_cmd.sendall(MPX_CMD('SET','CONTINUOUSRW,'+setCONTINUOUSRW[0]))
    #Set dynamic range
    # s_cmd.sendall(MPX_CMD('SET','COUNTERDEPTH,12'))
    s_cmd.sendall(MPX_CMD('SET','COUNTERDEPTH,'+setCOUNTERDEPTH))
    #Set frame time in miliseconds
    # s_cmd.sendall(MPX_CMD('SET','ACQUISITIONTIME,0.8'))
    s_cmd.sendall(MPX_CMD('SET','ACQUISITIONTIME,'+setACQUISITIONTIME))
    #Set gap time in milliseconds (The number fire corresponds to sum of frame and gap time)
    # s_cmd.sendall(MPX_CMD('SET','ACQUISITIONPERIOD,0.8'))
    s_cmd.sendall(MPX_CMD('SET','ACQUISITIONPERIOD,'+setACQUISITIONPERIOD))
    #Set number of frames to be acquired
    s_cmd.sendall(MPX_CMD('SET','NUMFRAMESTOACQUIRE,'+str(numberFrames)))
    s_cmd.sendall(MPX_CMD('SET','NUMFRAMESPERTRIGGER,'+str(nunFrmperTri)))
    # set trigger mode
    s_cmd.sendall(MPX_CMD('SET','TRIGGERSTART,'+triggers_dropdown))
    #Disable file saving
    # s_cmd.sendall(MPX_CMD('SET','FILEENABLE,0'))
    s_cmd.sendall(MPX_CMD('SET','FILEENABLE,'+setFILESAVING[0]))

    # get status from to detector
    # s_cmd.sendall(MPX_CMD('SET','HVBIAS,0'))
    s_cmd.sendall(MPX_CMD('GET','HVBIAS'))
    currentHVBIAS = s_cmd.recv(1024)
    currentHVBIAS = currentHVBIAS.decode()
    # s_cmd.sendall(MPX_CMD('SET','THRESHOLD0,0'))
    s_cmd.sendall(MPX_CMD('GET','THRESHOLD0'))
    currentTHRESHOLD0 = s_cmd.recv(1024)
    currentTHRESHOLD0 = currentTHRESHOLD0.decode()
    # Set continuous mode on
    # s_cmd.sendall(MPX_CMD('SET','CONTINUOUSRW,1'))
    s_cmd.sendall(MPX_CMD('GET','CONTINUOUSRW'))
    currentCONTINUOUSRW = s_cmd.recv(1024)
    currentCONTINUOUSRW = currentCONTINUOUSRW.decode()
    #Set dynamic range
    # s_cmd.sendall(MPX_CMD('SET','COUNTERDEPTH,12'))
    s_cmd.sendall(MPX_CMD('GET','COUNTERDEPTH'))
    currentCOUNTERDEPTH = s_cmd.recv(1024)
    currentCOUNTERDEPTH = currentCOUNTERDEPTH.decode()
    #Set frame time in miliseconds
    # s_cmd.sendall(MPX_CMD('SET','ACQUISITIONTIME,0.8'))
    s_cmd.sendall(MPX_CMD('GET','ACQUISITIONTIME'))
    currentACQUSITIONTIME = s_cmd.recv(1024)
    currentACQUSITIONTIME = currentACQUSITIONTIME.decode()
    #Set gap time in milliseconds (The number fire corresponds to sum of frame and gap time)
    # s_cmd.sendall(MPX_CMD('SET','ACQUISITIONPERIOD,0.8'))
    s_cmd.sendall(MPX_CMD('GET','ACQUISITIONPERIOD'))
    currentACQUSITIONPERIOD = s_cmd.recv(1024)
    currentACQUSITIONPERIOD = currentACQUSITIONPERIOD.decode()
    #Set number of frames to be acquired
    s_cmd.sendall(MPX_CMD('GET','NUMFRAMESTOACQUIRE'))
    currentNUMBERFRAME = s_cmd.recv(1024)
    currentNUMBERFRAME = currentNUMBERFRAME.decode()
    #Disable file saving
    # s_cmd.sendall(MPX_CMD('SET','FILEENABLE,0'))
    s_cmd.sendall(MPX_CMD('GET','FILEENABLE'))
    currentFILEENABLE = s_cmd.recv(1024)
    currentFILEENABLE = currentFILEENABLE.decode()

    s_cmd.sendall(MPX_CMD('GET','TRIGGERSTART'))
    currentTriggermode = s_cmd.recv(1024)
    currentTriggermode = currentTriggermode.decode()

    parent_conn.send(('configure', s_data, s_cmd, numberFrames))

    return currentHVBIAS, currentTHRESHOLD0, currentCOUNTERDEPTH, currentACQUSITIONTIME, currentACQUSITIONPERIOD, currentNUMBERFRAME, currentTriggermode

# callback to take data and receiving data using single process.
# integrated virtual detector images will be send back from data_worker.
@app_main.callback(
    # Output('eigfig1', 'figure'),
    Output('data_realspace_images_all', 'data'),
    # Output('realspaceimgs', 'children'),
    Output('realimgBF', 'figure'),
    Output('realimgABF', 'figure'),
    Output('realimg_ADF', 'figure'),
    Output('realimg_HAADF', 'figure'),
    Input('take4Dimage_stdalone', 'n_clicks'),
    # State('vd_list', 'value'),
    # State('boolean_BFVD', 'value'),
    # State('boolean_ABFVD', 'value'),
    # State('boolean_ADFVD', 'value'),
    # State('boolean_HAADFVD', 'value'),
    State('boolean_BFVD', 'on'),
    State('boolean_ABFVD', 'on'),
    State('boolean_ADFVD', 'on'),
    State('boolean_HAADFVD', 'on'),
    State('num_real_frm_perlines', 'value'),
    # State('num_eig', 'value'),
    prevent_initial_call=True
)
def merlin_take_data_stdalone(aa, bb, cc, dd, ee, ff):
    global HOST, COMMANDPORT, DATAPORT, s_cmd, numberFrames
    global s_data, parent_conn, child_conn
    vd_ls = []
    for vvtt, vvnn in zip([bb, cc, dd, ee], ['BF', 'ABF','ADF', 'HAADF']):
        if vvtt:
            vd_ls.append(vvnn)

    print(vd_ls) # for debug
    # send message to data_worker and start acqusition.
    parent_conn.send(('take4ddata_serial', vd_ls))
    # wait and receiv data from the data worker.
    # the received data is a list of lists, each inner list is the pixel values
    # for the type of virtual detectors.
    # The virtual detectors used is determined from the checkbox.
    # This function will take again the state of the virtual detector checkbox,
    # and assign to the
    data_ret = parent_conn.recv()
    print(data_ret[0])

    outdic = {'BF':[], 'ABF':[], 'ADF':[], 'HAADF':[]}
    for jj, vvdd in enumerate(vd_ls):
        fig1 = px.imshow(np.array(data_ret[jj]).reshape(ff, -1), color_continuous_scale = 'gray')
        outdic[vvdd] = fig1
    # return [
    # html.Div(children=dcc.Graph(figure=px.imshow(data_ret[0].reshape(256, 256))))
    # ]
    return outdic, outdic['BF'], outdic['ABF'], outdic['ADF'], outdic['HAADF']

# Call back for updating the selected virtual detector real space image.
# This separated real space image is needed because this one will be used to
# select single pixel for showing the reciprocal space image and
# select area for ML.
@app_main.callback(
    # Output('eigfig1', 'figure'),
    # Output('realspaceimgs', 'children'),
    Output('selected_realimg2', 'figure'),
    Input('realimg_dropdown', 'value'),
    # Input('realimg_dropdown', 'label'),
    State('data_realspace_images_all', 'data'),
    # State('vd_list', 'value'),
    # State('num_eig', 'value'),
    prevent_initial_call=True
)
def update_selected_realimg(aa, bb):
    print(aa)
    # return bb[aa]
    # fig1 = px.imshow(np.array(bb[aa]), color_continuous_scale = 'gray')
    # return fig1

# callback for saving data using this interface
@app_main.callback(
    # Output('eigfig1', 'figure'),
    # Output('realspaceimgs', 'children'),
    Output('data_socket_store_4ddatasaving', 'data'),
    Input('interfaceSaveData', 'n_clicks'),
    State('interfaceFilePath', 'value'),
    # State('vd_list', 'value'),
    # State('num_eig', 'value'),
    prevent_initial_call=True
)
def interface4d_datasaving(aa, bb):
    global s_data, parent_conn, child_conn
    parent_conn.send(('save4ddatatopkl', bb))

# callback to take 4d-stem data using parallel processing (computing integrated images)
@app_main.callback(
    # Output('eigfig1', 'figure'),
    Output('data_socket_store_take4d2', 'data'),
    Input('take4Dimage_stdalone2', 'n_clicks'),
    # State('num_eig', 'value'),
    prevent_initial_call=True
)
def merlin_take_data_stdalone2(aa,):
    global HOST, COMMANDPORT, DATAPORT, s_cmd, numberFrames
    global s_data, parent_conn, child_conn

    parent_conn.send(('take4ddata_para',))

# callback to set masks for virtual detectors 
@app_main.callback(
    # Output('eigfig1', 'figure'),
    Output('data_socket_store_setmasks', 'data'),
    Input('set_masks', 'n_clicks'),
    [
    Input('BFVD-range-slider', 'value'),
    Input('ABFVD-range-slider', 'value'),
    Input('ADFVD-range-slider', 'value'),
    Input('HAADFVD-range-slider', 'value'),
    # Input('ADFradiusin', 'value'),
    # Input('ADFradiusout', 'value'),
    # Input('HAADFradiusin', 'value'),
    # Input('HAADFradiusout', 'value'),
    ],
    # State('vd_list', 'value'),
    prevent_initial_call=True
)
def set_masks(aa, bb, cc, dd, ee, 
# ff, gg, hh, ii
):
    global HOST, COMMANDPORT, DATAPORT, s_cmd, numberFrames
    global s_data, parent_conn, child_conn

    # parent_conn.send(('set_masks', [[bb[0], bb[1]],
    #                                 [bb[2], bb[3]],
    #                                 [bb[4], bb[5]],
    #                                 [bb[6], bb[7]]]))
    parent_conn.send(('set_masks', [[bb[0], bb[1]],
                                    [cc[0], cc[1]],
                                    [dd[0], dd[1]],
                                    [ee[0], ee[1]]]))

# callback to load deep learning atom position identification model
@app_main.callback(
    # Output('eigfig1', 'figure'),
    Output('data_socket_store_loaddeepmodel', 'data'),
    Input('LoadDeepModel', 'n_clicks'),
    prevent_initial_call=True
)
def loaddeepmodel(aa):
    global HOST, COMMANDPORT, DATAPORT, s_cmd, numberFrames
    global s_data, parent_conn, child_conn

    parent_conn.send(('loaddeepmodel',))


# callback to load a example image for deep learning atom position identification
@app_main.callback(
    # Output('eigfig1', 'figure'),
    Output('atomseg_realimg', 'figure'),
    Input('LoadExampleImageDeep', 'n_clicks'),
    prevent_initial_call=True
)
def loadexpimgdeepmodel(aa):
    global HOST, COMMANDPORT, DATAPORT, s_cmd, numberFrames
    global s_data, parent_conn, child_conn
    global img_for_NN_atomIdent
    parent_conn.send(('loadexampledeepmodel',))
    img_for_NN_atomIdent = parent_conn.recv()
    fig = px.imshow(img_for_NN_atomIdent, color_continuous_scale = 'gray')
    return fig

# callback to run the image through the NN for atom postion identification
@app_main.callback(
    # Output('eigfig1', 'figure'),
    Output('atomseg_realimg_out', 'figure'),
    Input('rundeepatomseg', 'n_clicks'),
    # State('atomseg_realimg', 'figure'),
    prevent_initial_call=True
)
def NNatomIdent(aa):
    global HOST, COMMANDPORT, DATAPORT, s_cmd, numberFrames
    global s_data, parent_conn, child_conn
    global img_for_NN_atomIdent

    parent_conn.send(('nnatomident',))
    ret_atom_pos = parent_conn.recv()
    fig1 = px.imshow(img_for_NN_atomIdent, color_continuous_scale = 'gray')
    fig2 = px.imshow(ret_atom_pos[0], color_continuous_scale = 'gray')
    # fig = go.Figure(data=fig1.data + fig2.data)
    return fig2

# callback to load sample data with image for sivm
@app_main.callback(
    # Output('eigfig1', 'figure'),
    # Output('selected_realimg', 'figure'),
    Output('selected_realimg2', 'figure'),
    Output('selected_realimg3', 'figure'),
    # Output('data_socket_store_temp1', 'data'),
    Input('loadsamplesivm', 'n_clicks'),
    # State('atomseg_realimg', 'figure'),
    prevent_initial_call=True
)
def loadsivmsampledata(aa):
    global HOST, COMMANDPORT, DATAPORT, s_cmd, numberFrames
    global s_data, parent_conn, child_conn
    parent_conn.send(('loadsivmsampledata',))
    data_ret = parent_conn.recv()
    fig1 = px.imshow(np.array(data_ret).reshape(266,266), color_continuous_scale = 'gray') # reshape is based on known data set.
    fig1.update_layout(
                    dragmode='drawrect',
                    newshape=dict(
                                # fillcolor="cyan",
                                # opacity=0.3,
                                line=dict(color="yellow", width=3)
                                ),
                    )
    return fig1, fig1
    # return px.imshow(np.ones((10,10)))

# Callback for selected point on real space image
@app_main.callback(
    Output('recpimg', 'figure'),
    Input('selected_realimg2', 'clickData'),
    prevent_initial_call=True
    )
def update_rec_img(clickData):
    global parent_conn, child_conn
    # print(clickData)
    x_sel = clickData['points'][0]['x']
    y_sel = clickData['points'][0]['y']
    print(x_sel, y_sel)
    parent_conn.send(('sivmrealimgclick', x_sel, y_sel))
    ret_dat = parent_conn.recv()
    return px.imshow(ret_dat, color_continuous_scale = 'gray')

# Callback for get selected region of interest for decompostion
@app_main.callback(
# Output('select_range', 'children'),
Output('x_left', 'children'),
Output('intermediate_value_x_l', 'children'),
Output('x_right', 'children'),
Output('intermediate_value_x_r', 'children'),
Output('y_bottom', 'children'),
Output('intermediate_value_y_b', 'children'),
Output('y_top', 'children'),
Output('intermediate_value_y_t', 'children'),
# Output('status_bar', 'children'),
Input('selected_realimg2', 'relayoutData'),
prevent_initial_call=True
)
def update_sivm2(relayoutData):
    print(f'###################\n {relayoutData}')
    if "shapes" in relayoutData:
        last_shape = relayoutData["shapes"][-1]
        # shape coordinates are floats, we need to convert to ints for slicing
        x0, y0 = int(last_shape["x0"]), int(last_shape["y0"])
        x1, y1 = int(last_shape["x1"]), int(last_shape["y1"])
        if x0>x1:
            x0, x1 = x1, x0
        if y0>y1:
            y0, y1 = y1, y0
        print(type(x0))
        return x0, x0, x1, x1, y0, y0, y1, y1

    else:
        return 'zoom out', 'zoom out', 'zoom out', 'zoom out', 'zoom out', 'zoom out', 'zoom out', 'zoom out'

# Callback for get selected region of interest for projection
@app_main.callback(
# Output('select_range', 'children'),
Output('x_left_p', 'children'),
Output('intermediate_value_x_l_p', 'children'),
Output('x_right_p', 'children'),
Output('intermediate_value_x_r_p', 'children'),
Output('y_bottom_p', 'children'),
Output('intermediate_value_y_b_p', 'children'),
Output('y_top_p', 'children'),
Output('intermediate_value_y_t_p', 'children'),
# Output('status_bar', 'children'),
Input('selected_realimg3', 'relayoutData'),
prevent_initial_call=True
)
def update_proj(relayoutData):
    print(f'###################\n {relayoutData}')
    if "shapes" in relayoutData:
        last_shape = relayoutData["shapes"][-1]
        # shape coordinates are floats, we need to convert to ints for slicing
        x0, y0 = int(last_shape["x0"]), int(last_shape["y0"])
        x1, y1 = int(last_shape["x1"]), int(last_shape["y1"])
        if x0>x1:
            x0, x1 = x1, x0
        if y0>y1:
            y0, y1 = y1, y0
        print(type(x0))
        return x0, x0, x1, x1, y0, y0, y1, y1

    else:
        return 'zoom out', 'zoom out', 'zoom out', 'zoom out', 'zoom out', 'zoom out', 'zoom out', 'zoom out'

# Callback for decomposition and updating eigen images.
@app_main.callback(
    # Output('eigfig1', 'figure'),
    Output('ListOfEigen', 'children'),
    # Output('memory_eigen_loading', 'data'),
    Input('sivm_button', 'n_clicks'),
    State('intermediate_value_x_l', 'children'),
    State('intermediate_value_x_r', 'children'),
    State('intermediate_value_y_t', 'children'),
    State('intermediate_value_y_b', 'children'),
    State('num_eig', 'value'),
    prevent_initial_call=True
)
def update_eig_fig_cmd(n_clicks, input_x_l, input_x_r, input_y_t, input_y_b, eig_dim):
    parent_conn.send(('sivmdecompose', input_x_l, input_x_r, input_y_t, input_y_b, eig_dim))
    ret_dat = parent_conn.recv()
    # return ret_dat[0], ret_dat[1]
    # return ([html.Div(
    # children=dcc.Graph(figure=px.imshow(ii.reshape(144,144)))) for ii in ret_dat[0]], ret_dat[1])
    return [html.Div(
    children=dcc.Graph(figure=px.imshow(ii, color_continuous_scale = 'gray'))) for ii in ret_dat[0]]



###########################
# Callback for projecting and updating real-space images (selected area).
@app_main.callback(
    # Output('eigfig1', 'figure'),
    Output('ListOfProj', 'children'),
    Input('proj_button', 'n_clicks'),
    # State('memory_eigen_loading', 'data'),
    State('intermediate_value_x_l_p', 'children'),
    State('intermediate_value_x_r_p', 'children'),
    State('intermediate_value_y_t_p', 'children'),
    State('intermediate_value_y_b_p', 'children'),
    State('num_eig', 'value'),
    prevent_initial_call=True
)
def update_proj_fig(n_clicks_p, input_x_l_p, input_x_r_p, input_y_t_p, input_y_b_p, eig_dim):
    parent_conn.send(('sivmreproj', input_x_l_p, input_x_r_p, input_y_t_p, input_y_b_p, eig_dim))
    ret_dat = parent_conn.recv()
    return [html.Div(
    children=dcc.Graph(figure=px.imshow(ii, color_continuous_scale = 'gray'))) for ii in ret_dat[0]]
    # # length of selected projection area
    # x_len = input_x_r_p - input_x_l_p
    # y_len = input_y_t_p - input_y_b_p

    # # initialize data array
    # pj_data = []
    # print('before dc_data')
    # for xx in range(input_x_l_p, input_x_r_p):
    #     for yy in range(input_y_b_p, input_y_t_p):
    #         pj_data.append(f['Experiments']['planview scan 10um CA']['data'][xx,yy,:,:].flatten())
    # # dc_data = f['Experiments']['planview scan 10um CA']['data'][xx,yy,:,:]
    # pj_data = np.array(pj_data)
    # sivm_mdl_pj = pymf.NMF(pj_data.T, num_bases=eig_dim, niter=1, compW=False)
    # # sivm_mdl_pj.initialization()
    # sivm_mdl_pj.W = eigen_data
    # sivm_mdl_pj.factorize()

    # return [
    # # html.Div(children=dcc.Graph(figure=px.imshow(sivm_mdl_pj.H[ii].reshape(x_len,y_len)))) for ii in range(0, eig_dim)
    # html.Div(children=dcc.Graph(figure=px.imshow(sivm_mdl_pj.H[ii].reshape(y_len,x_len)))) for ii in range(0, eig_dim)
    # ]

if __name__ == "__main__":
    global HOST, COMMANDPORT, DATAPORT, s_cmd, numberFrames
    global s_data, parent_conn, child_conn

    HOST = '127.0.0.1'  # The server's hostname or IP address
    COMMANDPORT = 6341
    DATAPORT = 6342

    # s_cmd = socket.socket(socket.AF_INET, socket.SOCK_STREAM) # Create command socket
    # # Connect sockets and probe for the detector status
    # # try:
    # s_cmd.connect((HOST, COMMANDPORT))
    # #     # s_cmd.sendall(MPX_CMD('GET','SOFTWAREVERSION'))
    # #     # version = s_cmd.recv(1024)
    # #     # s_cmd.sendall(MPX_CMD('GET','DETECTORSTATUS'))
    # #     # status = s_cmd.recv(1024)
    # #     # print('Version CMD:', version.decode())
    # #     # print('Status CMD:', status.decode())
    # # except ConnectionRefusedError:
    # #     print("Merlin not responding")
    # # except OSError:
    # #     print("Merlin already connected")
    #
    #
    # # Connect data socket
    # s_data = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # # try:
    # # s_data.connect((HOST, DATAPORT))
    # s_data.connect((HOST, 6342))

    numberFrames = 65536
    parent_conn, child_conn = Pipe()

    # launch the data process and pass the command and data socket directly.
    # p = mp.Process(target=data_worker.main_worker,
    # args=(s_data, s_cmd, numberFrames, child_conn))
    # launch the data process only.
    p = mp.Process(target=data_worker.main_worker,
    args=(child_conn,))
    # s_data_in, s_cmd_in, numFrame_in, pipe_in
    p.start()
    # p.join()

    app_main.run_server(debug=True, port=8020)
    # app_main.run_server(debug=False, port=8020)
