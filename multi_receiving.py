import multiprocessing as mp
import socket
import numpy as np
import time

mp.allow_connection_pickling()

# set up command connection


tcpfrmdata_len = 15+384+131072

def init_worker(arr_in):
    global sum_img
    sum_img = arr_in

def worker1(ii):
    sum_img[ii] = ii+3

def sum_worker2(rawbytes, ii):
    framedata = rawbytes[15:]
    framesum = np.frombuffer(rawbytes[15+384:], dtype='>u2', count=-1)
    # sum_img[int(framedata[4:10].decode())] = np.sum(framesum, dtype='int64')+1
    #int_arr[int(framedata[4:10].decode())] = 333
    sum_img[ii] = ii

def sum_worker3(conn, mm):
    rawbytes = conn.recv(tcpfrmdata_len)
    while (len(rawbytes)!= tcpfrmdata_len):
        #print("\tframe",x,"partially received with length",len(framedata))
        # rawbytes += s_data.recv(tcpfrmdata_len-len(rawbytes))
        rawbytes += conn.recv(tcpfrmdata_len-len(rawbytes))
    framedata = rawbytes[15:]
    framesum = np.frombuffer(rawbytes[15+384:], dtype='>u2', count=-1)
    # sum_img[int(framedata[4:10].decode())] = int(framedata[4:10].decode())
    sum_img[int(framedata[4:10].decode())-1] = np.sum(framesum, dtype='int64')
    # print(int(framedata[4:10].decode()))
    # print(framedata[:10])
    print(f'worker {mp.current_process()} finished frme {int(framedata[4:10].decode())}')



if __name__ == '__main__':

    HOST = '127.0.0.1'  # The server's hostname or IP address
    COMMANDPORT = 6341    # The port used to send commands

    # convert command to generate TCP/IP Merlin string
    def MPX_CMD(type_cmd='GET',cmd='DETECTORSTATUS'):
        '''Generate TCP command string for Merlin software. type_cmd and cmd are documented in MerlinEM Documentation. Default value GET,DETECTORSTATUS probes for the current status of the detector.'''
        length = len(cmd)
        tmp = 'MPX,00000000' + str(length+5) + ',' + type_cmd + ',' + cmd
        return tmp.encode()

    s_cmd = socket.socket(socket.AF_INET, socket.SOCK_STREAM) # Create command socket
    # Connect sockets and probe for the detector status
    try:
        s_cmd.connect((HOST, COMMANDPORT))
        # s_cmd.sendall(MPX_CMD('GET','SOFTWAREVERSION'))
        # version = s_cmd.recv(1024)
        # s_cmd.sendall(MPX_CMD('GET','DETECTORSTATUS'))
        # status = s_cmd.recv(1024)
        # print('Version CMD:', version.decode())
        # print('Status CMD:', status.decode())
    except ConnectionRefusedError:
        print("Merlin not responding")
    except OSError:
        print("Merlin already connected")

    # Connect data socket
    DATAPORT = 6342        # The port used to receive the data
    s_data = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        s_data.connect((HOST, DATAPORT))
    except ConnectionRefusedError:
        print("Data port not responding")

    # Set base Merlin imaging parameters
    numberFrames = 500
    s_cmd.sendall(MPX_CMD('SET','HVBIAS,0'))
    s_cmd.sendall(MPX_CMD('SET','THRESHOLD0,0'))
    # s_cmd.sendall(MPX_CMD('SET','THRESHOLD1,511'))
    #Set continuous mode on
    s_cmd.sendall(MPX_CMD('SET','CONTINUOUSRW,1'))
    #Set dynamic range
    s_cmd.sendall(MPX_CMD('SET','COUNTERDEPTH,12'))
    #Set frame time in miliseconds
    s_cmd.sendall(MPX_CMD('SET','ACQUISITIONTIME,0.8'))
    #Set gap time in milliseconds (The number fire corresponds to sum of frame and gap time)
    s_cmd.sendall(MPX_CMD('SET','ACQUISITIONPERIOD,0.8'))
    #Set number of frames to be acquired
    s_cmd.sendall(MPX_CMD('SET','NUMFRAMESTOACQUIRE,'+str(numberFrames)))
    #Disable file saving
    s_cmd.sendall(MPX_CMD('SET','FILEENABLE,0'))

    #Start acquisition
    s_cmd.sendall(MPX_CMD('CMD','STARTACQUISITION'))

    # check TCP header for acquisition header (hdr) file
    data = s_data.recv(14)
    start = data.decode()
    # add the rest
    header = s_data.recv(int(start[4:]))
    if (len(header)==int(start[4:])):
        print("Header data received0. standaloe")

    # for x in range(numberFrames):
    #     tcpheader = s_data.recv(14)
    #     framedata = s_data.recv(int(tcpheader[4:]))
    #     while (len(framedata)!= int(tcpheader[4:])):
    #         #print("\tframe",x,"partially received with length",len(framedata))
    #         framedata += s_data.recv(int(tcpheader[4:])-len(framedata))
    # print(numberFrames,"frames received.")



    sum_img = mp.Array('Q', numberFrames) # use 'Q' for unsigned long long

    with mp.Pool(mp.cpu_count(), initializer=init_worker, initargs=(sum_img,)) as pool:
        # for jj in range(10):
            # r = pool.apply_async(worker1, (jj,))
            # r.get()
        # print(list(sum_img))

        prcs_all = []
        for ii in range(numberFrames):
            #rawbyte = s_data.recv(15+384+131072)
            #r = pool.apply_async(sum_worker2, args=(rawbyte, ii,))
            r = pool.apply_async(sum_worker3, args=(s_data, ii,))
            # prcs_all.append(pool.apply_async(sum_worker3, args=(s_data, ii,)))
            # r.wait()
            r.get()
        # for r in prcs_all:
        #     r.wait()
            # r.get()
        # prcs_all[-1].get()
        # r.wait()
    time.sleep(5)
    print(list(sum_img))
