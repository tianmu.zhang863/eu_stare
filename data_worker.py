# As we have decided to change the structure of the code, we will need
# a separate py file which will be running on another process different from
# the Dash process.


from cmath import exp
import re
import numpy as np
import numpy.ma as ma

# imports for retreiving data from server
import multiprocessing as mp
from multiprocessing import Process, Pipe, allow_connection_pickling

# import multiprocess as mp
# from multiprocess import Process, Pipe
# import multiprocess as mp2
import socket

import time
import select

# import psutil
import os

import pickle


import atomai as aoi
import PIL.Image as Image

import h5py
import pymf


mp.allow_connection_pickling()

# Defining global varibles
global s_data, s_cmd, numFrame

# raw 4d data storage
global data_4d

global atomposmodel, img_deep_pos


# function for converting to tcpip command
def MPX_CMD(type_cmd='GET',cmd='DETECTORSTATUS'):
    '''Generate TCP command string for Merlin software.
    type_cmd and cmd are documented in MerlinEM Documentation.
    Default value GET,DETECTORSTATUS probes for the current status of the detector.'''
    length = len(cmd)
    tmp = 'MPX,00000000' + str(length+5) + ',' + type_cmd + ',' + cmd
    return tmp.encode()

# mask function definition
# Credit: Lucas Brauch
def mask_gen(a, r1, r2, center=False):
    """takes a 2d-array shape, creates annular mask with specified radii and the returns the masked array
    a: Dimension tuple
    r1,r2: inner and outer radii in pixel (inner radius is included)
    center: center by pixel coordinate (x,y). Auto centers to middle if not specified"""
    #auto set center
    if center==False:
        center=[a[0]/2-0.5,a[1]/2-0.5]
    #create coordinate grid
    y, x = np.ogrid[:a[0], :a[1]]
    #create distance array
    dis = np.sqrt((x - center[0])**2 + (y-center[1])**2) #create boolean mask. Masked where True!
    m =~((r1 <= dis) & (dis < r2))
    return m

def generate_masks(radii_in):
    # the order will always be BF, ABF, ADF, HAADF
    # global maskBF, maskABF, maskADF, maskHAADF
    global mask_dic
    maskBF = mask_gen((256,256), radii_in[0][0], radii_in[0][1])
    maskABF = mask_gen((256,256), radii_in[1][0], radii_in[1][1])
    maskADF = mask_gen((256,256), radii_in[2][0], radii_in[2][1])
    maskHAADF = mask_gen((256,256), radii_in[3][0], radii_in[3][1])
    mask_dic = {'BF':maskBF, 'ABF':maskABF,
                'ADF':maskADF, 'HAADF':maskHAADF}
    print('masked generate')

# main worker function, respoinsible for receiving message and calling
# other workers.
# def main_worker(s_data_in, s_cmd_in, numFrame_in, pipe_in):
def main_worker(pipe_in):
    '''
    This is the main worker, when the process is launched from Dash process
    this will be the target funciton.
    This funciton will receive orders from Dash process and calling other functions
    in this .py file to perform tasks such as take data
    '''
    global s_data, s_cmd, numFrame, pipe_conn
    # s_data, s_cmd, numFrame, pipe_conn = s_data_in, s_cmd_in, numFrame_in, pipe_in
    pipe_conn = pipe_in

    # while loop for receiving message from pipe
    msg = 'wait_cmd'
    while 1:
        msg = pipe_conn.recv() # receive message

        # if message is passing s_cmd, s_data and numFrame
        if msg[0] == 'configure':
            s_data = msg[1]
            s_cmd = msg[2]
            numFrame = msg[3]

        if msg[0] == 'set_masks':
            generate_masks(msg[1])

        # if message is take data and receive serially
        if msg[0] == 'take4ddata_serial':
            vd_ls = msg[1]
            # print(vd_ls)
            aa = standaloneworker(s_cmd, s_data, numFrame, vd_ls)
        # if message is take data and receive using multiporcessing
        if msg[0] == 'take4ddata_para':
            aa = stdaloneworker3(s_cmd, s_data, numFrame)

        # if message is to save 4d data to pickle
        if msg[0] == 'save4ddatatopkl':
            interface4ddatasaving(msg[1])

        if msg[0] == 'loaddeepmodel':
            loaddeepmode()
            print('default deep learning model loaded')

        if msg[0] == 'loadexampledeepmodel':
            loaddeepexpimg()
            print('example deep learning model loaded')
        
        if msg[0] == 'nnatomident':
            nnatompospred()
            print('atom position predicted')

        if msg[0] == 'loadsivmsampledata':
            loadsivmexpdata()

        if msg[0] == 'sivmrealimgclick':
            sivmselsinglerecpimg(msg[1], msg[2])

        if msg[0] == 'sivmdecompose':
            update_eig_fig(msg[1], msg[2], msg[3], msg[4], msg[5])
        
        if msg[0] == 'sivmreproj':
            update_proj_fig(msg[1], msg[2], msg[3], msg[4], msg[5])


# function for receiving merlin data serially
def standaloneworker(s_cmd, s_data, numFrame, vd_ls):
    global  pipe_conn, mask_dic, data_4d
    data_4d = {}
    tcpfrmdata_len = 15+384+131072
    s_cmd.sendall(MPX_CMD('CMD','STARTACQUISITION'))

    data = s_data.recv(14)
    # ready = select.select([s_data], [], [], )
    # if ready[0]:
        # rawbytes = s_data.recv(tcpfrmdata_len)
        # data = s_data.recv(14)
    start = data.decode()
    header = s_data.recv(int(start[4:]))
    if (len(header)==int(start[4:])):
        print("Header data received.")

    # s_cmd.sendall(MPX_CMD('GET','DETECTORSTATUS'))
    # status = s_cmd.recv(1024)
    # print('Status CMD:', status.decode())

    sum_imgBF = []
    sum_imgABF = []
    sum_imgADF = []
    sum_imgHAADF = []

    sum_imgs = [[],[],[],[]]

    for ii in range(numFrame):
        # if the following line is used to check the availability of the data,
        # then indent the s_data.recv line.
        # ready = select.select([s_data], [], [], ) # check if there is data is available
        # if ready[0]:
        rawbytes = s_data.recv(tcpfrmdata_len)

        # s_cmd.sendall(MPX_CMD('GET','DETECTORSTATUS'))
        # status = s_cmd.recv(1024)
        # print('Status CMD:', status.decode())

        while (len(rawbytes)!= tcpfrmdata_len):
            # time.sleep(.5)

            # s_cmd.sendall(MPX_CMD('GET','DETECTORSTATUS'))
            # status = s_cmd.recv(1024)
            # print('Status CMD:', status.decode())

            rawbytes += s_data.recv(tcpfrmdata_len-len(rawbytes))
            # print(f'IN WHILE loop worker {mp.current_process()} received {len(rawbytes)} bytes')
        framedata = rawbytes[15:]
        framesum = np.frombuffer(rawbytes[15+384:], dtype='>u2', count=-1)
        data_4d[(0, ii)] = framesum
        for jj, msk in enumerate(vd_ls):
            frame_masked = ma.array(framesum, mask=mask_dic[msk])
            sum_imgs[jj].append(frame_masked.sum(dtype=np.int64))
        # sum_img.append(np.sum(framesum, dtype='int64'))
        # sum_img[int(framedata[4:10].decode())] = int(framedata[4:10].decode())
        # sum_img[int(framedata[4:10].decode())-1] = np.sum(framesum, dtype='int64')
    # print(sum_imgs[0])
    pipe_conn.send(sum_imgs)
    return None


###############################
# Multiprocessing receiving worker
# init_worker1:
def init_worker1(arr_in, data_sck, cmd_sck, lk_in):
    # sum_img: shared array for real space image
    # data_sck: data socket
    # lk_in: multiprocessing lock
    global sum_img, s_data, llkk, s_cmd
    sum_img = arr_in
    s_data = data_sck
    s_cmd = cmd_sck
    llkk = lk_in

    p = psutil.Process(os.getpid())
    p.nice(psutil.REALTIME_PRIORITY_CLASS)

# pre-calculate the total byte length for one frame
tcpfrmdata_len = 15+384+131072
# define the multiprocessing worker,
# This worker f'n prints out, for debug purpose, the length of received data.
def sum_worker1():
    global tcpfrmdata_len

    # s_cmd.sendall(MPX_CMD('GET','DETECTORSTATUS'))
    # status = s_cmd.recv(1024)
    # print('Status CMD:', status.decode())


    llkk.acquire() # acquire lock for the data socket.
    # if the following line is used to check the availability of the data,
    # then indent the s_data.recv line.
    # ready = select.select([s_data], [], [], ) # check if there is data is available
    # if ready[0]:
    rawbytes = s_data.recv(tcpfrmdata_len)

    # s_cmd.sendall(MPX_CMD('GET','DETECTORSTATUS'))
    # status = s_cmd.recv(1024)
    # print('Status CMD:', status.decode())

    # print(f'worker {mp.current_process()} received {len(rawbytes)} bytes')
    while (len(rawbytes)!= tcpfrmdata_len):
        # time.sleep(.5)

        # s_cmd.sendall(MPX_CMD('GET','DETECTORSTATUS'))
        # status = s_cmd.recv(1024)
        # print('Status CMD:', status.decode())

        rawbytes += s_data.recv(tcpfrmdata_len-len(rawbytes))
        # print(f'IN WHILE loop worker {mp.current_process()} received {len(rawbytes)} bytes')
    llkk.release() # release the lock for data socket.
    framedata = rawbytes[15:]
    framesum = np.frombuffer(rawbytes[15+384:], dtype='>u2', count=-1)
    # sum_img[int(framedata[4:10].decode())] = int(framedata[4:10].decode())
    sum_img[int(framedata[4:10].decode())-1] = np.sum(framesum, dtype='int64')
    # print(f'worker {mp.current_process()} finished frme {int(framedata[4:10].decode())}')

def stdaloneworker3(s_cmd, s_data, numberFrames):
# def merlin_take_data(aa, ):
    # global HOST, COMMANDPORT, s_cmd, s_data, numberFrames

    sum_img = mp.Array('Q', numberFrames) # use 'Q' for unsigned long long
    l = mp.Lock()
    s_cmd.sendall(MPX_CMD('CMD','STARTACQUISITION'))

    # s_data_ = s_data
    data = s_data.recv(14)
    # ready = select.select([s_data], [], [], )
    # if ready[0]:
        # rawbytes = s_data.recv(tcpfrmdata_len)
        # data = s_data.recv(14)
    start = data.decode()
    header = s_data.recv(int(start[4:]))
    if (len(header)==int(start[4:])):
        print("Header data received.")


    with mp.Pool(mp.cpu_count()-4, initializer=init_worker1, initargs=(sum_img, s_data, s_cmd, l)) as pool:
        prcs_all = []
        for ii in range(numberFrames):
            r = pool.apply_async(sum_worker1, args=())
            r.get()
            # pool.apply(sum_worker1, args=())
        # for r in prcs_all:
        #     r.get()
            # r.wait()
        # prcs_all[-1].wait()
        # prcs_all[-1].get()
    print(list(sum_img))

    return None

def interface4ddatasaving(pp):
    global data_4d, numFrame
    out_arr = np.zeros((numFrame, 256*256))
    for ii in range(numFrame):
        out_arr[ii] = data_4d[(0, ii)]
    np.save(pp, out_arr)


def loaddeepmode():
    global atomposmodel
    atomposmodel = aoi.load_model('./assets/deep/model_metadict_final.tar')

def loaddeepexpimg():
    global atomposmodel, img_deep_pos
    # atomposmodel = aoi.load_model('./assets/deep/model_metadict_final.tar')
    imarr1 = Image.open('./assets/deep/01adf (Auto Aligned).tif')
    img_deep_pos = np.array(imarr1)
    pipe_conn.send(img_deep_pos)
    # return 

def nnatompospred():
    global atomposmodel, img_deep_pos
    nnout1 = atomposmodel.predict(img_deep_pos)
    # pipe_conn.send(nnout1[1])
    pipe_conn.send(nnout1[0])
    # print(nnout1)
    # return 

def sivmexpmultiinit():
    global data_4d
    exp4dfile = './assets/sample_data/HfO2 planview2 new_05_05_2020 18_42_06.blo.hdf5_float64_centered.hdf5'
    data_4d = h5py.File(exp4dfile, 'r', driver='core')
    data_4d = data_4d['Experiments']['planview scan 10um CA']['data']
    # data_4d = np.array(data_4d)

# def sivmexp_iterworker(xx):
#     return [np.sum(ii) for ii in xx]
def sivmexp_iterworker(xx):
    return np.sum(data_4d[xx])
def loadsivmexpdata():
    global data_4d
    if os.path.exists('./assets/sample_data/sivmsampleintegrate.npy'):
        pool_results = np.load('./assets/sample_data/sivmsampleintegrate.npy',)
        sivmexpmultiinit()
    else:
        with mp.Pool(initializer=sivmexpmultiinit, initargs=(), processes=10) as pool:
            pool_results = pool.map(sivmexp_iterworker, [(xx, yy) for xx in range(266) for yy in range(266)])
        # pool.close()
        # pool.join()
        np.save('./assets/sample_data/sivmsampleintegrate.npy', np.array(pool_results))
    pipe_conn.send(pool_results)
    print('loading example sivm data finished')
    # print(pool_results)

def sivmselsinglerecpimg(xx, yy):
    global data_4d, pipe_conn
    pipe_conn.send(data_4d[xx, yy, :, :])
    # return data_4d[xx, yy]

def update_eig_fig(
                # n_clicks, 
                input_x_l, input_x_r, input_y_t, input_y_b, eig_dim):
    global data_4d, pipe_conn, eigen_data
    # dc_data = np.array([])
    dc_data = []
    print('before dc_data')
    print(input_x_l, input_x_r, input_y_b)
    # print(x_l, x_r, y_b, y_t)
    print(type(input_x_l), input_x_r, input_y_t, input_y_b)
    for xx in range(input_x_l, input_x_r):
        for yy in range(input_y_b, input_y_t):
            # dc_data.append(f['Experiments']['planview scan 10um CA']['data'][xx,yy,:,:].flatten())
            dc_data.append(data_4d[xx,yy,:,:].flatten())
    # dc_data = f['Experiments']['planview scan 10um CA']['data'][xx,yy,:,:]
    dc_data = np.array(dc_data)
    print(dc_data.shape)
    sivm_mdl = pymf.SIVM(dc_data.T, num_bases=eig_dim)
    print(1)
    # sivm_mdl.initialization()
    sivm_mdl.factorize()
    print(2)
    eigen_data = sivm_mdl.W
    # return px.imshow(sivm_mdl.H[0].reshape(144,144))
    # pipe_conn.send(([sivm_mdl.W.T[ii].reshape(144,144) for ii in range(0, eig_dim)], sivm_mdl.W))
    pipe_conn.send(([sivm_mdl.W.T[ii].reshape(144,144) for ii in range(0, eig_dim)], 1))

def update_proj_fig(
    # n_clicks_p, eigen_data,
    input_x_l_p, input_x_r_p, input_y_t_p, input_y_b_p, eig_dim):
    global data_4d, pipe_conn, eigen_data
    # length of selected projection area
    x_len = input_x_r_p - input_x_l_p
    y_len = input_y_t_p - input_y_b_p

    # initialize data array
    pj_data = []
    print('before dc_data')
    for xx in range(input_x_l_p, input_x_r_p):
        for yy in range(input_y_b_p, input_y_t_p):
            pj_data.append(data_4d[xx,yy,:,:].flatten())
    # dc_data = f['Experiments']['planview scan 10um CA']['data'][xx,yy,:,:]
    pj_data = np.array(pj_data)
    sivm_mdl_pj = pymf.NMF(pj_data.T, num_bases=eig_dim, niter=1, compW=False)
    # sivm_mdl_pj.initialization()
    sivm_mdl_pj.W = eigen_data
    sivm_mdl_pj.factorize()
    # pipe_conn.send(([sivm_mdl_pj.H[ii].reshape(y_len,x_len) for ii in range(0, eig_dim)], 1))
    pipe_conn.send(([sivm_mdl_pj.H[ii].reshape(x_len,y_len).T for ii in range(0, eig_dim)], 1))
    # return [
    # # html.Div(children=dcc.Graph(figure=px.imshow(sivm_mdl_pj.H[ii].reshape(x_len,y_len)))) for ii in range(0, eig_dim)
    # html.Div(children=dcc.Graph(figure=px.imshow(sivm_mdl_pj.H[ii].reshape(y_len,x_len)))) for ii in range(0, eig_dim)
    # ]