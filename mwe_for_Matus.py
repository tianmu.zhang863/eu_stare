########################################
########################################
# STARE project 4D-STEM with SimplexMax factorizartion file
#
# By Tianmu Zhang, Alex Zintler, Lucas Brauch, Tianshu Jiang, Oscar Recalde and Leopoldo Molina-Luna
# AEM @ TU Darmstadt
# tianmu.zhang@tu-darmstadt.de
# alex.zintler@aem.tu-darmstadt.de

# The purpose of this file is to run our app on a different workstation.
# This contains a Dash app, the app will have two main funcions, thus two buttons.
# The first button is used for connect the Dash app to the Merlin server,
# it will also establish the command and data sockets, send configuration
# commands to Merlin.
# The second button sends the "STARTACQUISITION" command, then the Dash app
# will start a mutliprocessing worker pool.
# In this version no virtual detectors are implemented, workers will simply
# add the counts in all of the reciprocal space pixels together.



# imports for dash and plotly
import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output, State, ALL, MATCH
import plotly.express as px

# imports for other utility parts
import numpy as np
import json


# imports for retreiving data from server
import multiprocessing as mp
import socket

import time
import select

import psutil
import os

import stdaloneworker

mp.allow_connection_pickling()

# init_worker1:
def init_worker1(arr_in, data_sck, cmd_sck, lk_in):
    # sum_img: shared array for real space image
    # data_sck: data socket
    # lk_in: multiprocessing lock
    global sum_img, s_data, llkk, s_cmd
    sum_img = arr_in
    s_data = data_sck
    s_cmd = cmd_sck
    llkk = lk_in

    p = psutil.Process(os.getpid())
    p.nice(psutil.REALTIME_PRIORITY_CLASS)

# init_worker2:
# this initialier has one more parameter than 1, the firstFlag is used
# to tell whether the receivning process has began.
# If not, the first worker has to take the bytes only for the acquisition header.
# Then the flag will be reset and the rest workers will read bytes for frames.
def init_worker2(arr_in, data_sck, cmd_sck, lk_in, flg_in):
    # sum_img: shared array for real space image
    # data_sck: data socket
    # lk_in: multiprocessing lock
    # firstFlag: flag for whether the receiving process has began.
    global sum_img, s_data, llkk, s_cmd, firstFlag
    sum_img = arr_in
    s_data = data_sck
    s_cmd = cmd_sck
    llkk = lk_in
    firstFlag = flg_in

    p = psutil.Process(os.getpid())
    p.nice(psutil.REALTIME_PRIORITY_CLASS)


# pre-calculate the total byte length for one frame
tcpfrmdata_len = 15+384+131072
# define the multiprocessing worker,
# This worker f'n prints out, for debug purpose, the length of received data.
def sum_worker1():
    global tcpfrmdata_len

    # s_cmd.sendall(MPX_CMD('GET','DETECTORSTATUS'))
    # status = s_cmd.recv(1024)
    # print('Status CMD:', status.decode())


    llkk.acquire() # acquire lock for the data socket.
    # if the following line is used to check the availability of the data,
    # then indent the s_data.recv line.
    # ready = select.select([s_data], [], [], ) # check if there is data is available
    # if ready[0]:
    rawbytes = s_data.recv(tcpfrmdata_len)

    # s_cmd.sendall(MPX_CMD('GET','DETECTORSTATUS'))
    # status = s_cmd.recv(1024)
    # print('Status CMD:', status.decode())

    # print(f'worker {mp.current_process()} received {len(rawbytes)} bytes')
    while (len(rawbytes)!= tcpfrmdata_len):
        time.sleep(.5)

        # s_cmd.sendall(MPX_CMD('GET','DETECTORSTATUS'))
        # status = s_cmd.recv(1024)
        # print('Status CMD:', status.decode())

        rawbytes += s_data.recv(tcpfrmdata_len-len(rawbytes))
        # print(f'IN WHILE loop worker {mp.current_process()} received {len(rawbytes)} bytes')
    llkk.release() # release the lock for data socket.
    framedata = rawbytes[15:]
    framesum = np.frombuffer(rawbytes[15+384:], dtype='>u2', count=-1)
    # sum_img[int(framedata[4:10].decode())] = int(framedata[4:10].decode())
    sum_img[int(framedata[4:10].decode())-1] = np.sum(framesum, dtype='int64')
    # print(f'worker {mp.current_process()} finished frme {int(framedata[4:10].decode())}')

def sum_worker2():
    global tcpfrmdata_len
    global firstFlag

    # s_cmd.sendall(MPX_CMD('GET','DETECTORSTATUS'))
    # status = s_cmd.recv(1024)
    # print('Status CMD:', status.decode())


    llkk.acquire() # acquire lock for the data socket.
    # if the following line is used to check the availability of the data,
    # then indent the s_data.recv line.
    ready = select.select([s_data], [], [], ) # check if there is data is available
    while not ready[0]:
        ready = select.select([s_data], [], [], )
    # if ready[0]:

    if firstFlag:
        # pass
        data = s_data.recv(14)
        firstFlag = False
        # ready = select.select([s_data], [], [], )
        # if ready[0]:
            # rawbytes = s_data.recv(tcpfrmdata_len)
            # data = s_data.recv(14)
        start = data.decode()
        llkk.release()
        header = s_data.recv(int(start[4:]))
        if (len(header)==int(start[4:])):
            print("Header data received.")

    else:
        rawbytes = s_data.recv(tcpfrmdata_len)

        # s_cmd.sendall(MPX_CMD('GET','DETECTORSTATUS'))
        # status = s_cmd.recv(1024)
        # print('Status CMD:', status.decode())

        # print(f'worker {mp.current_process()} received {len(rawbytes)} bytes')
        while (len(rawbytes)!= tcpfrmdata_len):
            # time.sleep(.5)

            # s_cmd.sendall(MPX_CMD('GET','DETECTORSTATUS'))
            # status = s_cmd.recv(1024)
            # print('Status CMD:', status.decode())

            rawbytes += s_data.recv(tcpfrmdata_len-len(rawbytes))
            # print(f'IN WHILE loop worker {mp.current_process()} received {len(rawbytes)} bytes')
        llkk.release() # release the lock for data socket.
        framedata = rawbytes[15:]
        framesum = np.frombuffer(rawbytes[15+384:], dtype='>u2', count=-1)
        # sum_img[int(framedata[4:10].decode())] = int(framedata[4:10].decode())
        sum_img[int(framedata[4:10].decode())-1] = np.sum(framesum, dtype='int64')
        print(f'worker {mp.current_process()} finished frme {int(framedata[4:10].decode())}')


def standaloneworker(s_cmd, s_data, numFrame):
    tcpfrmdata_len = 15+384+131072
    s_cmd.sendall(MPX_CMD('CMD','STARTACQUISITION'))

    # s_data_ = s_data
    data = s_data.recv(14)
    # ready = select.select([s_data], [], [], )
    # if ready[0]:
        # rawbytes = s_data.recv(tcpfrmdata_len)
        # data = s_data.recv(14)
    start = data.decode()
    header = s_data.recv(int(start[4:]))
    if (len(header)==int(start[4:])):
        print("Header data received.")

    # s_cmd.sendall(MPX_CMD('GET','DETECTORSTATUS'))
    # status = s_cmd.recv(1024)
    # print('Status CMD:', status.decode())

    sum_img = []

    for ii in range(numFrame):
        # if the following line is used to check the availability of the data,
        # then indent the s_data.recv line.
        # ready = select.select([s_data], [], [], ) # check if there is data is available
        # if ready[0]:
        rawbytes = s_data.recv(tcpfrmdata_len)

        # s_cmd.sendall(MPX_CMD('GET','DETECTORSTATUS'))
        # status = s_cmd.recv(1024)
        # print('Status CMD:', status.decode())

        while (len(rawbytes)!= tcpfrmdata_len):
            time.sleep(.5)

            # s_cmd.sendall(MPX_CMD('GET','DETECTORSTATUS'))
            # status = s_cmd.recv(1024)
            # print('Status CMD:', status.decode())

            rawbytes += s_data.recv(tcpfrmdata_len-len(rawbytes))
            # print(f'IN WHILE loop worker {mp.current_process()} received {len(rawbytes)} bytes')
        framedata = rawbytes[15:]
        framesum = np.frombuffer(rawbytes[15+384:], dtype='>u2', count=-1)
        sum_img.append(np.sum(framesum, dtype='int64'))
        # sum_img[int(framedata[4:10].decode())] = int(framedata[4:10].decode())
        # sum_img[int(framedata[4:10].decode())-1] = np.sum(framesum, dtype='int64')

    print(sum_img)
    return sum_img

def standaloneworker2(s_cmd, s_data, numFrame):
    s_cmd.sendall(MPX_CMD('CMD','STARTACQUISITION'))

    # s_data_ = s_data
    data = s_data.recv(14)
    # ready = select.select([s_data], [], [], )
    # if ready[0]:
        # rawbytes = s_data.recv(tcpfrmdata_len)
        # data = s_data.recv(14)
    start = data.decode()
    header = s_data.recv(int(start[4:]))
    if (len(header)==int(start[4:])):
        print("Header data received.")

    sum_img = mp.Array('Q', numberFrames) # use 'Q' for unsigned long long
    l = mp.Lock()

    with mp.Pool(mp.cpu_count()-1, initializer=init_worker1, initargs=(sum_img, s_data, s_cmd, l)) as pool:
        prcs_all = []
        for ii in range(numberFrames):
            r = pool.apply_async(sum_worker1, args=())
            r.get()
            # pool.apply(sum_worker1, args=())
        # for r in prcs_all:
        #     r.get()
            # r.wait()
        # prcs_all[-1].wait()
        # prcs_all[-1].get()
    print(list(sum_img))

    return None

def MPX_CMD(type_cmd='GET',cmd='DETECTORSTATUS'):
    '''Generate TCP command string for Merlin software.
    type_cmd and cmd are documented in MerlinEM Documentation.
    Default value GET,DETECTORSTATUS probes for the current status of the detector.'''
    length = len(cmd)
    tmp = 'MPX,00000000' + str(length+5) + ',' + type_cmd + ',' + cmd
    return tmp.encode()

# main dash app starts from here
app_main = dash.Dash(__name__)

app_main.layout = html.Div(
    children = [

        # storages for outputs from callback functions.
        # They are here since callback functions need output.
        dcc.Store(id='data_socket_store'),
        dcc.Store(id='data_socket_store2'),
        dcc.Store(id='data_socket_store3'),
        dcc.Store(id='data_socket_store4'),
        dcc.Store(id='data_socket_store5'),
        dcc.Store(id='data_socket_store6'),

        #################################
        html.Div(
        children = [
        html.Div(
            children=['number of lines: ',
            dcc.Input(id='num_real_img_lines', type='number', placeholder='number of lines',
                        value=512)]
                ),
        html.Div(
            children=['number of frames per lines: ',
            dcc.Input(id='num_real_frm_perlines', type='number', placeholder='number of frames',
                        value=512)]
                ),
        html.Div(children=[
            html.Button(id='config_serv_button', n_clicks=0, children=['Configure Server'])
                ]),
        html.Div(children=[
            html.Button(id='take4Dimage', n_clicks=0, children=['take 4d image'] ),
                ]),
        html.Div(children=[
            html.Button(id='take4Dimage_para2', n_clicks=0, children=['take 4d image the second way'] ),
                ]),
        html.Div(children=[
            html.Button(id='take4Dimage_nonpara', n_clicks=0, children=['take 4d image non parallel'] ),
                ]),
        html.Div(children=[
            html.Button(id='take4Dimage_stdalone', n_clicks=0, children=['take 4d image standalone'] ),
                ]),
        html.Div(children=[
            html.Button(id='take4Dimage_stdalone2', n_clicks=0, children=['take 4d image standalone2'] ),
                ]),
        ]
        ),
        ])

@app_main.callback(
    # Output('eigfig1', 'figure'),
    Output('data_socket_store5', 'data'),
    Input('take4Dimage_stdalone', 'n_clicks'),
    # State('num_eig', 'value'),
    prevent_initial_call=True
)
def merlin_take_data_stdalone(aa,):
    global HOST, COMMANDPORT, DATAPORT, s_cmd, numberFrames
    global s_data
    p = mp.Process(target=standaloneworker,
    args=(s_cmd, s_data, numberFrames))
    p.start()
    p.join()
    # standaloneworker(s_cmd, s_data, numFrame)

@app_main.callback(
    # Output('eigfig1', 'figure'),
    Output('data_socket_store6', 'data'),
    Input('take4Dimage_stdalone2', 'n_clicks'),
    # State('num_eig', 'value'),
    prevent_initial_call=True
)
def merlin_take_data_stdalone2(aa,):
    global HOST, COMMANDPORT, DATAPORT, s_cmd, numberFrames
    global s_data
    p = mp.Process(target=stdaloneworker.stdaloneworker3,
    args=(s_cmd, s_data, numberFrames))
    p.start()
    p.join()

# Callback for configure Merlin button
@app_main.callback(
    # Output('eigfig1', 'figure'),
    Output('data_socket_store', 'data'),
    Input('config_serv_button', 'n_clicks'),
    # State('num_eig', 'value'),
    prevent_initial_call=True
)
def configure_server(aa,):
    # global varibles are needed, so the socket can be kept outside callback
    # functions.
    global HOST, COMMANDPORT, DATAPORT, s_cmd, numberFrames
    global s_data
    s_cmd = socket.socket(socket.AF_INET, socket.SOCK_STREAM) # Create command socket
    # Connect sockets and probe for the detector status
    # try:
    s_cmd.connect((HOST, COMMANDPORT))
    #     # s_cmd.sendall(MPX_CMD('GET','SOFTWAREVERSION'))
    #     # version = s_cmd.recv(1024)
    #     # s_cmd.sendall(MPX_CMD('GET','DETECTORSTATUS'))
    #     # status = s_cmd.recv(1024)
    #     # print('Version CMD:', version.decode())
    #     # print('Status CMD:', status.decode())
    # except ConnectionRefusedError:
    #     print("Merlin not responding")
    # except OSError:
    #     print("Merlin already connected")
    # s_cmd.close()


    # Connect data socket
    s_data = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # try:
    # s_data.connect((HOST, DATAPORT))
    s_data.connect((HOST, 6342))
    # s_data.setblocking(0)
    # except ConnectionRefusedError:
    #     print("Data port not responding")
    # Set base Merlin imaging parameters
    numberFrames = 65500

    s_cmd.sendall(MPX_CMD('SET','HVBIAS,0'))
    s_cmd.sendall(MPX_CMD('SET','THRESHOLD0,0'))
    # s_cmd.sendall(MPX_CMD('SET','THRESHOLD1,511'))

    # Set continuous mode on
    s_cmd.sendall(MPX_CMD('SET','CONTINUOUSRW,1'))
    #Set dynamic range
    s_cmd.sendall(MPX_CMD('SET','COUNTERDEPTH,12'))
    #Set frame time in miliseconds
    s_cmd.sendall(MPX_CMD('SET','ACQUISITIONTIME,0.8'))
    #Set gap time in milliseconds (The number fire corresponds to sum of frame and gap time)
    s_cmd.sendall(MPX_CMD('SET','ACQUISITIONPERIOD,0.8'))
    #Set number of frames to be acquired
    s_cmd.sendall(MPX_CMD('SET','NUMFRAMESTOACQUIRE,'+str(numberFrames)))
    #Disable file saving
    s_cmd.sendall(MPX_CMD('SET','FILEENABLE,0'))

    return None

# Callback for Merlin_take_data button
@app_main.callback(
    # Output('eigfig1', 'figure'),
    Output('data_socket_store2', 'data'),
    Input('take4Dimage', 'n_clicks'),
    # State('num_eig', 'value'),
    prevent_initial_call=True
)
def merlin_take_data(aa, ):
    global HOST, COMMANDPORT, s_cmd, s_data, numberFrames

    s_cmd.sendall(MPX_CMD('CMD','STARTACQUISITION'))

    # s_data_ = s_data
    data = s_data.recv(14)
    # ready = select.select([s_data], [], [], )
    # if ready[0]:
        # rawbytes = s_data.recv(tcpfrmdata_len)
        # data = s_data.recv(14)
    start = data.decode()
    header = s_data.recv(int(start[4:]))
    if (len(header)==int(start[4:])):
        print("Header data received.")

    sum_img = mp.Array('Q', numberFrames) # use 'Q' for unsigned long long
    l = mp.Lock()

    with mp.Pool(mp.cpu_count()-1, initializer=init_worker1, initargs=(sum_img, s_data, s_cmd, l)) as pool:
        prcs_all = []
        for ii in range(numberFrames):
            r = pool.apply_async(sum_worker1, args=())
            r.get()
            # pool.apply(sum_worker1, args=())
        # for r in prcs_all:
        #     r.get()
            # r.wait()
        # prcs_all[-1].wait()
        # prcs_all[-1].get()
    print(list(sum_img))

    return None

@app_main.callback(
    # Output('eigfig1', 'figure'),
    Output('data_socket_store4', 'data'),
    Input('take4Dimage_para2', 'n_clicks'),
    # State('num_eig', 'value'),
    prevent_initial_call=True
)
def merlin_take_data_para2(aa, ):
    global HOST, COMMANDPORT, s_cmd, s_data, numberFrames

    sum_img = mp.Array('Q', numberFrames) # use 'Q' for unsigned long long
    l = mp.Lock()
    firstFlag = True
    pool = mp.Pool(mp.cpu_count(), initializer=init_worker2, initargs=(sum_img, s_data, s_cmd, l, firstFlag))
    # arr_in, data_sck, cmd_sck, lk_in, flg_in
    prcs_all = []
    for ii in range(numberFrames):
        # r = pool.apply_async(sum_worker2, args=())
        prcs_all.append(pool.apply_async(sum_worker2, args=()))
        # r.get()
        # r.wait()

    pool.close()
    # pool.join()
    s_cmd.sendall(MPX_CMD('CMD','STARTACQUISITION'))
    for ii in prcs_all:
        ii.get()
    # pool.join()

    # with mp.Pool(mp.cpu_count()-5, initializer=init_worker1, initargs=(sum_img, s_data, s_cmd, l)) as pool:

    print(list(sum_img))

    return None


# Callback for Merlin_take_data button, non parallel way.
@app_main.callback(
    # Output('eigfig1', 'figure'),
    Output('data_socket_store3', 'data'),
    Input('take4Dimage_nonpara', 'n_clicks'),
    # State('num_eig', 'value'),
    prevent_initial_call=True
)
def merlin_take_data_nonpara(aa, ):
    global HOST, COMMANDPORT, s_cmd, s_data, numberFrames
    global tcpfrmdata_len

    s_cmd.sendall(MPX_CMD('CMD','STARTACQUISITION'))

    # s_data_ = s_data
    data = s_data.recv(14)
    # ready = select.select([s_data], [], [], )
    # if ready[0]:
        # rawbytes = s_data.recv(tcpfrmdata_len)
        # data = s_data.recv(14)
    start = data.decode()
    header = s_data.recv(int(start[4:]))
    if (len(header)==int(start[4:])):
        print("Header data received.")

    # s_cmd.sendall(MPX_CMD('GET','DETECTORSTATUS'))
    # status = s_cmd.recv(1024)
    # print('Status CMD:', status.decode())

    sum_img = []

    for ii in range(numberFrames):
        # if the following line is used to check the availability of the data,
        # then indent the s_data.recv line.
        # ready = select.select([s_data], [], [], ) # check if there is data is available
        # if ready[0]:
        rawbytes = s_data.recv(tcpfrmdata_len)

        # s_cmd.sendall(MPX_CMD('GET','DETECTORSTATUS'))
        # status = s_cmd.recv(1024)
        # print('Status CMD:', status.decode())

        while (len(rawbytes)!= tcpfrmdata_len):
            time.sleep(.5)

            # s_cmd.sendall(MPX_CMD('GET','DETECTORSTATUS'))
            # status = s_cmd.recv(1024)
            # print('Status CMD:', status.decode())

            rawbytes += s_data.recv(tcpfrmdata_len-len(rawbytes))
            # print(f'IN WHILE loop worker {mp.current_process()} received {len(rawbytes)} bytes')
        framedata = rawbytes[15:]
        framesum = np.frombuffer(rawbytes[15+384:], dtype='>u2', count=-1)
        sum_img.append(np.sum(framesum, dtype='int64'))
        # sum_img[int(framedata[4:10].decode())] = int(framedata[4:10].decode())
        # sum_img[int(framedata[4:10].decode())-1] = np.sum(framesum, dtype='int64')


    return None

if __name__ == "__main__":
    global HOST, COMMANDPORT, DATAPORT, s_cmd, numberFrames
    global s_data

    HOST = '127.0.0.1'  # The server's hostname or IP address
    COMMANDPORT = 6341
    DATAPORT = 6342

    # app_main.run_server(debug=True, port=8020)
    app_main.run_server(debug=False, port=8020)
