########################################
########################################
# STARE project 4D-STEM with SimplexMax factorizartion file
#
# By Tianmu Zhang, Alex Zintler, Lucas Brauch, Tianshu Jiang, Oscar Recalde and Leopoldo Molina-Luna
# AEM @ TU Darmstadt
# tianmu.zhang@tu-darmstadt.de
# alex.zintler@aem.tu-darmstadt.de

# version 0.0
# Juli 2021, Darmstadt DE

# imports for dash and plotly
import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output, State, ALL, MATCH
import plotly.express as px


# imports for other utility parts
import numpy as np
import json
# import h5py
import pymf

# imports for retreiving data from server
import multiprocessing as mp
import socket


import time
import select

#################################
# create empty image array
x_size = 256
y_size = 256
real_img = np.zeros((x_size, y_size))
eig_img_test = np.zeros((x_size, y_size))
recp_img = np.array([])

# main data dic
# this dic will be used to store the reciprocal space frames.
# keys will be the frame sequence number
# the reason for doing this, (as opposed to use h5) is,
# we have enough memory on the Merlin PC, and this should be faster
# than h5.
main_data_dic = {}

config = {
    "modeBarButtonsToAdd": [
        "drawline",
        "drawopenpath",
        "drawclosedpath",
        "drawcircle",
        "drawrect",
        "eraseshape",
    ]
}


mp.allow_connection_pickling()

# global HOST, COMMANDPORT, s_cmd, numberFrames
# global s_data
# HOST = '127.0.0.1'  # The server's hostname or IP address
# COMMANDPORT = 6341    # The port used to send commands

# set up command connection
# s_cmd = socket.socket(socket.AF_INET, socket.SOCK_STREAM) # Create command socket
# # Connect sockets and probe for the detector status
# # try:
# s_cmd.connect((HOST, COMMANDPORT))

# set up initiizer for pool and worker function
# init_worker1: nur ein shared array
def init_worker1(arr_in):
    global sum_img
    sum_img = arr_in

# init_worker2: shared array and shared lock
def init_worker2(arr_in, lock_in):
    global sum_img, lk
    sum_img = arr_in
    lk = lock_in

# init_worker3: data socket as shared
def init_worker3(arr_in, data_sck, lk_in):
    global sum_img, s_data, llkk
    sum_img = arr_in
    s_data = data_sck
    llkk = lk_in

tcpfrmdata_len = 15+384+131072
# def sum_worker3(conn, mm):
def sum_worker3( mm):
    global tcpfrmdata_len
    # , s_data
    time.sleep(.2)
    llkk.acquire()
    ready = select.select([s_data], [], [], )
    if ready[0]:
        # data = mysocket.recv(4096)
        # rawbytes = conn.recv(tcpfrmdata_len)
        rawbytes = s_data.recv(tcpfrmdata_len)
    print(f'worker {mp.current_process()} received {len(rawbytes)} bytes')
    while (len(rawbytes)!= tcpfrmdata_len):
        # time.sleep(.5)
        # print("\tframe",x,"partially received with length",len(framedata))
        rawbytes += s_data.recv(tcpfrmdata_len-len(rawbytes))
        print(f'IN WHILE loop worker {mp.current_process()} received {len(rawbytes)} bytes')
        # rawbytes += conn.recv(tcpfrmdata_len-len(rawbytes))
    llkk.release()
    framedata = rawbytes[15:]
    framesum = np.frombuffer(rawbytes[15+384:], dtype='>u2', count=-1)
    # sum_img[int(framedata[4:10].decode())] = int(framedata[4:10].decode())
    sum_img[int(framedata[4:10].decode())-1] = np.sum(framesum, dtype='int64')

    # rawbytes = conn.recv(15+384+131072)
    # framedata = rawbytes[15:]
    # framesum = np.frombuffer(rawbytes[15+384:], dtype='>u2', count=-1)
    # sum_img[int(framedata[4:10].decode())] = int(framedata[4:10].decode())
    # sum_img[int(framedata[4:10].decode())-1] = np.sum(framesum, dtype='int64')
    # print(int(framedata[4:10].decode()))
    # print(framedata[:10])
    # print(f'{mp.current_process()} just finished')
    print(f'worker {mp.current_process()} finished frme {int(framedata[4:10].decode())}')

def sum_worker4(conn, mm):
    global tcpfrmdata_len
    lk.acquire()
    rawbytes = conn.recv(tcpfrmdata_len)
    while (len(rawbytes)!= tcpfrmdata_len):
        #print("\tframe",x,"partially received with length",len(framedata))
        # rawbytes += s_data.recv(tcpfrmdata_len-len(rawbytes))
        rawbytes += conn.recv(tcpfrmdata_len-len(rawbytes))
    lk.release()
    framedata = rawbytes[15:]
    framesum = np.frombuffer(rawbytes[15+384:], dtype='>u2', count=-1)
    # sum_img[int(framedata[4:10].decode())] = int(framedata[4:10].decode())
    sum_img[int(framedata[4:10].decode())-1] = np.sum(framesum, dtype='int64')
    print(f'worker {mp.current_process()} finished frme {int(framedata[4:10].decode())}')




# main dash app starts from here
app_main = dash.Dash(__name__)

app_main.layout = html.Div(
    children = [

        # storage for data socket, which is the output of the 'config_server'
        # button.
        dcc.Store(id='data_socket_store'),
        dcc.Store(id='data_socket_store2'),
        dcc.Store(id='data_socket_store3'),

        #################################
        # memory for decomposition result, this is generated from the decomposition
        # callback and will be used for the projection callback.
        dcc.Store(id='memory_eigen_loading'),

        #################################
        html.Div(
        children = [
        html.Div(
            children=['number of lines: ',
            dcc.Input(id='num_real_img_lines', type='number', placeholder='number of lines',
                        value=512)]
                ),
        html.Div(
            children=['number of frames per lines: ',
            dcc.Input(id='num_real_frm_perlines', type='number', placeholder='number of frames',
                        value=512)]
                ),
        html.Div(children=[
            html.Button(id='config_serv_button', n_clicks=0, children=['Configure Server'])
                ]),
        html.Div(children=[
            html.Button(id='take4Dimage', n_clicks=0, children=['take 4d image'] ),
                ]),
        html.Div(children=[
            html.Button(id='run_standalone', n_clicks=0, children=['run_standalone'] ),
                ]),
        ]
        ),

        html.Div(
                children =[
                            #################################
                            # two real-space images side by side, one for selecting decomposition region,
                            # the other for selecting projection region.
                            html.Div(
                            style={'width': '45%', 'height': '45%', 'display': 'inline-block'},
                            children = [html.Center('select decompo. region'),
                            dcc.Graph(id="graph",
                            # figure = fig1,
                            config=config),
                            dcc.Graph(id='recpimg')]
                            ),
                            html.Div(
                            style={'width': '45%', 'height': '45%', 'display': 'inline-block'},
                            children=[html.Center('select project. region'),
                            dcc.Graph(id="graph2",
                            # figure = fig1,
                            config=config)])
                            ]
                    ),

        #################################
        # input for selecting decompostion dimensions
        html.Div(
        children=['number of \n eigen/reduce_dim: ',
        dcc.Input(id='num_eig', type='number', placeholder='number of dim.')]
        ),

        html.Div(children=[
            #################################
            # two buttons, one for perform decompostion one for performing projection.

            #################################
            # Button decomposition
            html.Div(
            style={'width': '45%', 'height': '45%', 'display': 'inline-block'},
            children = [
            html.Button(id='sivm_button', n_clicks=0, children=[
                html.Div(children=[html.Div(
                children=['''x left: ''', html.Div(id = 'x_left',
                style={'display': 'inline-block'})], style={'display': 'inline-block'}),
                html.Div(id='intermediate_value_x_l', style={'display': 'none'})]),

                html.Div(children=[html.Div(
                children=['''x right: ''', html.Div(id = 'x_right',
                style={'display': 'inline-block'})], style={'display': 'inline-block'}),
                html.Div(id='intermediate_value_x_r', style={'display': 'none'})]),

                html.Div(children=[html.Div(
                children=['''y bottom: ''', html.Div(id = 'y_top',
                style={'display': 'inline-block'})], style={'display': 'inline-block'}),
                html.Div(id='intermediate_value_y_b', style={'display': 'none'})]),

                html.Div(children=[html.Div(
                children=['''y top: ''', html.Div(id = 'y_bottom',
                style={'display': 'inline-block'})], style={'display': 'inline-block'}),
                html.Div(id='intermediate_value_y_t', style={'display': 'none'})]),

                html.Div(children=['perform decomposition'])
                ])]
                ),

            #################################
            # Button projection
            html.Div(style={'width': '45%', 'height': '45%', 'display': 'inline-block'},
            children = [
            html.Button(id='proj_button', n_clicks=0, children=[
                html.Div(children=[html.Div(
                children=['''x left: ''', html.Div(id = 'x_left_p', style={'display': 'inline-block'})], style={'display': 'inline-block'}),
                html.Div(id='intermediate_value_x_l_p', style={'display': 'none'})]),

                html.Div(children=[html.Div(
                children=['''x right: ''', html.Div(id = 'x_right_p', style={'display': 'inline-block'})], style={'display': 'inline-block'}),
                html.Div(id='intermediate_value_x_r_p', style={'display': 'none'})]),

                html.Div(children=[html.Div(
                children=['''y bottom: ''', html.Div(id = 'y_top_p', style={'display': 'inline-block'})], style={'display': 'inline-block'}),
                html.Div(id='intermediate_value_y_b_p', style={'display': 'none'})]),

                html.Div(children=[html.Div(
                children=['''y top: ''', html.Div(id = 'y_bottom_p', style={'display': 'inline-block'})], style={'display': 'inline-block'}),
                html.Div(id='intermediate_value_y_t_p', style={'display': 'none'})]),

                html.Div(children=['perform projection'])
                ])]),
                ]),

        #################################
        # List of decompostion and projection images, side by side
        html.Div(
        children = [
                    html.Div(style={'width': '45%', 'height': '45%', 'display': 'inline-block'},
                    children=[
                            html.Div('List of eigen images'),
                            # html.Div(id='ListOfEigen', children=[])
                            html.Div(id='ListOfEigen',)
                            ]),
                    html.Div(style={'width': '45%', 'height': '45%', 'display': 'inline-block'},
                    children=[
                            html.Div('List of projection images'),
                            # html.Div(id='ListOfEigen', children=[])
                            html.Div(id='ListOfProj',)])
                    ]
                )
    ]
)


# convert command to generate TCP/IP Merlin string
def MPX_CMD(type_cmd='GET',cmd='DETECTORSTATUS'):
    '''Generate TCP command string for Merlin software.
    type_cmd and cmd are documented in MerlinEM Documentation.
    Default value GET,DETECTORSTATUS probes for the current status of the detector.'''
    length = len(cmd)
    tmp = 'MPX,00000000' + str(length+5) + ',' + type_cmd + ',' + cmd
    return tmp.encode()

@app_main.callback(
    # Output('eigfig1', 'figure'),
    Output('data_socket_store', 'data'),
    Input('config_serv_button', 'n_clicks'),
    # State('num_eig', 'value'),
    prevent_initial_call=True
)
def configure_server(aa,):
    global HOST, COMMANDPORT, s_cmd, s_data, numberFrames
    # global HOST, COMMANDPORT, s_cmd, numberFrames
    s_cmd = socket.socket(socket.AF_INET, socket.SOCK_STREAM) # Create command socket
    # Connect sockets and probe for the detector status
    # try:
    s_cmd.connect((HOST, COMMANDPORT))
    #     # time.sleep(1)
    #     # s_cmd.sendall(MPX_CMD('GET','SOFTWAREVERSION'))
    #     # I need to comment out the receiving command feedback for now,
    #     # because it many times the server will not repond properly to
    #     # this and the run of the code hangs...
    #     # time.sleep(1)
    #     # version = s_cmd.recv(1024)
    #     # s_cmd.sendall(MPX_CMD('GET','DETECTORSTATUS'))
    #     # status = s_cmd.recv(1024)
    #     # print('Version CMD:', version.decode())
    #     # print('Status CMD:', status.decode())
    #     # time.sleep(1)
    # except ConnectionRefusedError:
    #     print("Merlin not responding")
    # except OSError:
    #     print("Merlin already connected")
    # s_cmd.close()


    # Connect data socket
    DATAPORT = 6342        # The port used to receive the data
    s_data = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # try:
    # s_data.connect((HOST, DATAPORT))
    s_data.connect((HOST, 6342))
    s_data.setblocking(0)
    # except ConnectionRefusedError:
    #     print("Data port not responding")
    # Set base Merlin imaging parameters
    numberFrames = 100


    s_cmd.sendall(MPX_CMD('SET','HVBIAS,0'))
    s_cmd.sendall(MPX_CMD('SET','THRESHOLD0,0'))
    # s_cmd.sendall(MPX_CMD('SET','THRESHOLD1,511'))


    # Set continuous mode on
    s_cmd.sendall(MPX_CMD('SET','CONTINUOUSRW,1'))
    #Set dynamic range
    s_cmd.sendall(MPX_CMD('SET','COUNTERDEPTH,12'))
    #Set frame time in miliseconds
    s_cmd.sendall(MPX_CMD('SET','ACQUISITIONTIME,10'))
    #Set gap time in milliseconds (The number fire corresponds to sum of frame and gap time)
    s_cmd.sendall(MPX_CMD('SET','ACQUISITIONPERIOD,0.8'))
    #Set number of frames to be acquired
    s_cmd.sendall(MPX_CMD('SET','NUMFRAMESTOACQUIRE,'+str(numberFrames)))
    #Disable file saving
    s_cmd.sendall(MPX_CMD('SET','FILEENABLE,0'))

    # s_cmd.close()
    # print('command socket cl')

    #Start acquisition
    # s_cmd.sendall(MPX_CMD('CMD','STARTACQUISITION'))

    # s_cmd = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # s_cmd.connect((HOST, COMMANDPORT))
    # s_cmd.sendall(MPX_CMD('CMD','STARTACQUISITION'))
    # s_cmd.close()
    # # check TCP header for acquisition header (hdr) file
    # data = s_data.recv(14)
    # start = data.decode()
    # # add the rest
    # header = s_data.recv(int(start[4:]))
    # if (len(header)==int(start[4:])):
    #     print("Header data received.")

    # return s_cmd
    return None

@app_main.callback(
    # Output('eigfig1', 'figure'),
    Output('data_socket_store2', 'data'),
    Input('take4Dimage', 'n_clicks'),
    # State('num_eig', 'value'),
    prevent_initial_call=True
)
def merlin_take_data(aa, ):
    global HOST, COMMANDPORT, s_cmd, s_data, numberFrames
    # global HOST, COMMANDPORT, s_cmd, numberFrames
    # Connect data socket
    # DATAPORT = 6342        # The port used to receive the data
    # s_data = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # # try:
    # s_data.connect((HOST, DATAPORT))
    # except ConnectionRefusedError:
    #     print("Data port not responding")

    # s_cmd = socket.socket(socket.AF_INET, socket.SOCK_STREAM) # Create command socket
    # # Connect sockets and probe for the detector status
    # # try:
    # s_cmd.connect((HOST, COMMANDPORT))

    # Connect data socket
    # DATAPORT = 6342        # The port used to receive the data
    # s_data_ = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # # try:
    # # s_data.connect((HOST, DATAPORT))
    # s_data_.connect((HOST, 6342))
    # except ConnectionRefusedError:
    #     print("Data port not responding")
    # Set base Merlin imaging parameters
    # numberFrames = 500


    # s_cmd.sendall(MPX_CMD('SET','HVBIAS,0'))
    # s_cmd.sendall(MPX_CMD('SET','THRESHOLD0,0'))
    # # s_cmd.sendall(MPX_CMD('SET','THRESHOLD1,511'))
    #
    #
    # # Set continuous mode on
    # s_cmd.sendall(MPX_CMD('SET','CONTINUOUSRW,1'))
    # #Set dynamic range
    # s_cmd.sendall(MPX_CMD('SET','COUNTERDEPTH,12'))
    # #Set frame time in miliseconds
    # s_cmd.sendall(MPX_CMD('SET','ACQUISITIONTIME,0.8'))
    # #Set gap time in milliseconds (The number fire corresponds to sum of frame and gap time)
    # s_cmd.sendall(MPX_CMD('SET','ACQUISITIONPERIOD,0.8'))
    # #Set number of frames to be acquired
    # s_cmd.sendall(MPX_CMD('SET','NUMFRAMESTOACQUIRE,'+str(numberFrames)))
    # #Disable file saving
    # s_cmd.sendall(MPX_CMD('SET','FILEENABLE,0'))



    # s_cmd = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # s_cmd.connect((HOST, COMMANDPORT))
    s_cmd.sendall(MPX_CMD('CMD','STARTACQUISITION'))
    # s_cmd.close()

    s_data_ = s_data
    # data = s_data.recv(14)
    ready = select.select([s_data], [], [], )
    if ready[0]:
        # rawbytes = s_data.recv(tcpfrmdata_len)
        data = s_data_.recv(14)
    start = data.decode()
    # s_cmd.sendall(MPX_CMD('CMD','STARTACQUISITION'))
    # add the rest
    header = s_data_.recv(int(start[4:]))
    if (len(header)==int(start[4:])):
        print("Header data received.")

    # for x in range(numberFrames):
    #     tcpheader = s_data.recv(14)
    #     framedata = s_data.recv(int(tcpheader[4:]))
    #     while (len(framedata)!= int(tcpheader[4:])):
    #         #print("\tframe",x,"partially received with length",len(framedata))
    #         framedata += s_data.recv(int(tcpheader[4:])-len(framedata))
    # print(numberFrames,"frames received.")

    time.sleep(1)
    sum_img = mp.Array('Q', numberFrames) # use 'Q' for unsigned long long
    l = mp.Lock()

    # with mp.Pool(mp.cpu_count(), initializer=init_worker1, initargs=(sum_img,)) as pool:
    # with mp.Pool(mp.cpu_count(), initializer=init_worker3, initargs=(sum_img, s_data_, l)) as pool:
    with mp.Pool(8, initializer=init_worker3, initargs=(sum_img, s_data_, l)) as pool:
        # for jj in range(10):
            # r = pool.apply_async(worker1, (jj,))
            # r.get()
        # print(list(sum_img))

        prcs_all = []
        for ii in range(numberFrames):
            #rawbyte = s_data.recv(15+384+131072)
            #r = pool.apply_async(sum_worker2, args=(rawbyte, ii,))
            r = pool.apply_async(sum_worker3, args=( ii,))
            # prcs_all.append(pool.apply_async(sum_worker3, args=(s_data, ii,)))
            # r = pool.apply_async(sum_worker3, args=(s_data_, ii,))

            r.get()
        # for r in prcs_all:
        #     r.get()
            # r.wait()
        # prcs_all[-1].wait()
        # prcs_all[-1].get()
        # time.sleep(15)
        # print(prcs_all)
        # r.wait()
    print(list(sum_img))
    # s_data.close()

    return None
    # return s_data


@app_main.callback(
    # Output('eigfig1', 'figure'),
    Output('data_socket_store3', 'data'),
    Input('run_standalone', 'n_clicks'),
    # State('num_eig', 'value'),
    prevent_initial_call=True
)
def merlin_take_data2(aa, ):
    import os
    os.system('D:\Tianmu\merlincontrol\multi_receiving.py')
    # exec(open('D:\Tianmu\merlincontrol\multi_receiving.py').read())
    return None


if __name__ == "__main__":
    global HOST, COMMANDPORT, s_cmd, numberFrames
    # global s_data

    HOST = '127.0.0.1'  # The server's hostname or IP address
    COMMANDPORT = 6341

    # global s_cmd


    # check TCP header for acquisition header (hdr) file
    # data = s_data.recv(14)
    # start = data.decode()
    # add the rest
    # header = s_data.recv(int(start[4:]))
    # if (len(header)==int(start[4:])):
        # print("Header data received.")


    # sum_img = mp.Array('Q', numberFrames) # use 'Q' for unsigned long long


    app_main.run_server(debug=True, port=8020)
    # app_main.run_server(debug=False, port=8020)
    # app_main.server.run(host='0.0.0.0', port=8020)
